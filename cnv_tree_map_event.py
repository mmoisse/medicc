#!/usr/bin/python

'''
Created on 22 Sep 2011

@author: schwar01
'''
import sys
import Bio.Phylo
import commons.tools.log
import optparse
import cnvphylo

LOG = commons.tools.log.Log()


def main():
    usage = "USAGE: %prog <tree_file>"
    parser = optparse.OptionParser(usage=usage)
    
    parser.add_option("-i", "--input-format", action="store", dest="input_format", default="phyloxml",
                      metavar="FORMAT", help="input format (def: phyloxml")
    parser.add_option("-r", "--region", action="store", dest="regions", type="string",
                      help="comma seperated list of regions to include (1..n), if empty reads stdin",
                      default="")
    
    (options, args) = parser.parse_args()
    
    if len(args) != 1:
        parser.error("incorrect number of arguments")
    
    strSourceFile = args[0]
    
    ## parse region selection
    if options.regions=="":
        text = sys.stdin.readline().strip()
    else:
        text = options.regions.strip()
    
    cols = list()
    if text != "":
        regions_text = [c.strip() for c in text.strip().split(",")]
        for col in regions_text:
            try:
                val = int(col)
                cols.append(val)
            except Exception as ex:
                print >> sys.stderr, "Error parsing " + col + "!"
        
    if len(cols) == 0:
        print >> sys.stderr, "No columns selected!"
        sys.exit(1)

    tree = Bio.Phylo.read(strSourceFile, options.input_format)
    print " ".join([str(c) for c in cols])
    
    for clade in tree.find_clades():
        for seqobj in clade.sequences:
            id = seqobj.accession.value
            seq = "".join([s for s in seqobj.name if s!="X"])
            line = id
            for col in cols:
                if col <= len(seq):
                    line = line + " " + seq[col-1]
                else:
                    raise cnvphylo.CnvPhyloError("")
            print line    
# ======================================================================    

if __name__ == '__main__':
    sys.exit(main())
    
    
        