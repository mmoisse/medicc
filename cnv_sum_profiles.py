#!/usr/bin/python

'''
Created on 19 May 2011

@author: rfs

sums the copy number profiles of two binary transducers and outputs the result as a binary
transducer

'''

import cnvphylo
import sys
from optparse import OptionParser
from commons.tools import log
import os
import numpy
import Bio
import Bio.Phylo
import Bio.SeqIO
import Bio.SeqRecord
import Bio.Seq
import Bio.Alphabet
from cnvphylo import cnvtools
import fst.factory

LOG = log.Log()
CNV_SYMBOLS_FILE = "/home/rfs/Documents/Projects/CRI/project-cnv-distance/cnv.symbols"
ALPHABET = [c for c in "0123456789a"]

def main():
    """ the main method """
    
    usage = "USAGE: %prog <fst_1> <fst_2>"
    
    parser = OptionParser(usage)
    
    parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
                      default = False, help = "be verbose?")
    parser.add_option("-a", "--alphabet", action = "store", dest = "alphabet",
                      default = "01234567890abcdef", help = "alphabet to use")
    parser.add_option("-s", "--sequence", action = "store_true", dest = "is_sequence",
                      default = False, help = "source files are sequence files, not fsts")
    parser.add_option("-f", "--file-format", action = "store", dest = "file_format",
                      default = "fasta", help = "file format of the sequence file")
    
    
    (options, args) = parser.parse_args()
   
    if len(args) != 2:
        parser.error("incorrect number of arguments")
    
    LOG.is_logging = options.verbose
    
    input_file1 = args[0]
    input_file2 = args[1]
    
    if not os.path.exists(input_file1):
        print >> sys.stderr, "Input file 1 does not exist!"
        sys.exit(1)
    
    if not os.path.exists(input_file2):
        print >> sys.stderr, "Input file 2 does not exist!"
        sys.exit(1)

    if options.alphabet!="":
        alphabet = [c for c in options.alphabet]
    else:
        alphabet = None
        
    tb = cnvtools.CnvToolBox(cnvtools.CnvBaseConfig(".", cnv_symbol_file=CNV_SYMBOLS_FILE, alphabet=alphabet))
    
    if not options.is_sequence:
        result = tb.sum_sequences(input_file1, input_file2)
        sys.stdout.write(result)
    else:
        textFSTs = list()
        
        with open(input_file1, "r") as fd_major:
            seqs_major = [record for record in Bio.SeqIO.parse(fd_major, options.file_format)]
            idx_major = [s.id for s in seqs_major]
            
        with open(input_file2, "r") as fd_minor:
            seqs_minor = [record for record in Bio.SeqIO.parse(fd_minor, options.file_format)]
            idx_minor = [s.id for s in seqs_minor]

        if len(list(set(idx_major))) != len(idx_major):
            raise StandardError("FATAL: sequence ids not unique (in major cn file). Exiting.")

        if len(list(set(idx_minor))) != len(idx_minor):
            raise StandardError("FATAL: sequence ids not unique (in minor cn file). Exiting.")
        
        if sum([seqid not in idx_minor for seqid in idx_major]): ## list not identical
            raise StandardError("FATAL: sequence identifiers differ in major and minor cn files. Exiting.")
        
        for cur_major in seqs_major:
            cur_minor = seqs_minor[idx_minor.index(cur_major.id)]
            
            text_sum = "".join(tb.sum_text_sequences(cur_major, cur_minor))
            
            #fst_sum = fst.factory.FSTFactory.create_fixed_length_hmm_from_sequences([text_sum,], ALPHABET, pseudo_counts=0)
            
            label = cur_major.id.replace("/", "_") ## filenames mustn't contain "/"            
            textFSTs.append((label, text_sum))
                
        new_sequences = []
        for i in range(0, len(textFSTs)):
            newseq = Bio.Seq.Seq(textFSTs[i][1], Bio.Alphabet.Alphabet())
            new_sequences.append(Bio.SeqRecord.SeqRecord(newseq, textFSTs[i][0], textFSTs[i][0], textFSTs[i][0]))
        Bio.SeqIO.write(new_sequences, sys.stdout, "fasta")
        
    

if __name__ == '__main__':
    sys.exit(main())
   


