'''
Created on 26 Nov 2010

@author: rfs

'''
from commons.tools.log import Log
import os
import subprocess
from subprocess import Popen 
import shlex
import tempfile
from fst.factory import FSTFactory
from fst.fst import Fst 
import sys
from Bio import SeqIO, Phylo
from subprocess import CalledProcessError
import numpy
import scipy
from StringIO import StringIO
import fst.io 
import fst.algos
from cnvphylo import *
import threading
import shutil
import csv
import glob
import Bio.SeqIO
import platform


HOME = os.getenv("HOME")

ALIGN_BINARY = os.path.abspath(BIN_DIR + "/fst_align_std" + (".exe" if IS_WINDOWS else ""))
SP_BINARY = os.path.abspath(BIN_DIR + "/fst_sp_std" + (".exe" if IS_WINDOWS else ""))
COUNT_BINARY = os.path.abspath(BIN_DIR + "/fst_count_paths" + (".exe" if IS_WINDOWS else ""))
RANDGENW_BINARY = os.path.abspath(BIN_DIR + "/fst_randgenweighted" + (".exe" if IS_WINDOWS else ""))
PDT_INTERSECT_BINARY = os.path.abspath(BIN_DIR + "/pdtintersect" + (".exe" if IS_WINDOWS else ""))

SORT_NONE=0
SORT_FIRST=1
SORT_SECOND=2
SORT_OPTIONS = [SORT_NONE, SORT_FIRST, SORT_SECOND]

COMMAND_COMPOSE="fstcompose"
COMMAND_INTERSECT="fstintersect"
COMMAND_UNION="fstunion"
COMMAND_PDTINTERSECT = PDT_INTERSECT_BINARY
COMMAND_OPTIONS = [COMMAND_COMPOSE, COMMAND_INTERSECT, COMMAND_UNION, COMMAND_PDTINTERSECT]

class CnvToolBox(object):
    """ Toolbox next version for doing lots of basic things with CNV FSTs """
    
    def __init__(self, workingDir, configuration, logger=None):
        """ constructor """
        if not isinstance(configuration, CnvBaseConfig):
            raise CnvToolsError("Need configuration object of class CnvBaseConfig")
            
        self.config = configuration
        self.workingDir = workingDir
        if logger==None:
            self.log = Log(is_logging=True)
        else:
            self.log = logger

        
    def bmulticommand(self, command, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, 
                      sort=SORT_FIRST, repeat=1, return_filename=False):
        """ applies a command multiple times to multiple fsts or repeatedly to the same.
        Uses Cyril's "log" strategy to speed up computation. If return_filename is true it
        returns the name of the final temporary file istead of the binary fst."""
        
        self.log.write("Multicommand %s" % command)

        filenames = [self._input_filename(o) for o in inputfsts] * repeat
        
        if not sort in SORT_OPTIONS:
            raise CnvToolsError("Unknown sort parameter!")
        
        if not command in COMMAND_OPTIONS:
            raise CnvToolsError("Unknown command parameter!")
        
        remaining = filenames[:]
        level = 0
        tempfiles = list()
        while len(remaining)>1:
            next_level = remaining[:]
            for i in range(0,len(remaining)/2): ## build pairs
                file1 = remaining[2*i]
                file2 = remaining[2*i+1]
                
                next_level.remove(file1)
                next_level.remove(file2)
                
                p = []
                
                if os.path.dirname(file1)=="":
                    file1 = "%s/%s.bin" % (self.workingDir, file1) 
                if os.path.dirname(file2)=="":
                    file2 = "%s/%s.bin" % (self.workingDir, file2)
                    
                #self.log.writeln("Joining " + file1 + " and " + file2) 

                with tempfile.NamedTemporaryFile(delete=False) as fd_tmp:
                    if sort == SORT_FIRST:                    
                        p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' " + file1, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                        stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        p.append( Popen(shlex.split("%s - %s" % (command, file2), posix=not IS_WINDOWS), 
                                                    stdin = p[-1].stdout, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                    elif sort == SORT_SECOND:
                        p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' " + file2, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                        stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        p.append( Popen(shlex.split("%s %s -" % (command, file1), posix=not IS_WINDOWS), 
                                                    stdin = p[-1].stdout, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                    else:
                        if command == COMMAND_PDTINTERSECT:
                            p.append( Popen(shlex.split("%s %s %s %s" % (command, file1, file2, self.config.pdt_parens_file), posix=not IS_WINDOWS), 
                                stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        else:
                            p.append( Popen(shlex.split("%s %s %s" % (command, file1, file2), posix=not IS_WINDOWS), 
                                stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        
                    do_prune = False
                    if prune_weight >= 0 and prune_level <= level:
                        do_prune = True

                    if not detmin and do_prune: ## if we are determinizing use weighted determinization instead
                        p.append( Popen(shlex.split("fstprune --weight=%d - " 
                                                    % prune_weight, posix=not IS_WINDOWS), stdin = p[-1].stdout, stdout = subprocess.PIPE, 
                                                    stderr = subprocess.PIPE, shell=platform.system()=="Windows") )

                    if detmin:
                        p.append( Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
                                        stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        if do_prune:
                            p.append( Popen(shlex.split("fstdeterminize --weight=%d - " % prune_weight, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                            stdin = p[-1].stdout, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                        else:
                            p.append( Popen(shlex.split("fstdeterminize - ", posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                            stdin = p[-1].stdout, stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                            
                        p.append( Popen(shlex.split("fstminimize - ", posix=not IS_WINDOWS), stdin = p[-1].stdout, stdout = subprocess.PIPE, 
                                        stderr = subprocess.PIPE, shell=platform.system()=="Windows") )
                    self.log.write(".")
                    result = p[-1].communicate()
                    _cleanup(p)
                    fd_tmp.write(result[0])
                    
                    #self.log.write(self.binfo(result[0]))
                    #self.log.writeln("-----------------------------------------------------------------------------")

                tempfiles.append(fd_tmp.name)                
                next_level.append(fd_tmp.name)
            remaining = next_level
            level += 1
        
           
        self.log.writeln("done!")
        
        if return_filename:
            result = remaining[-1]
        else:
            with open(remaining[-1], "rb") as fd_last:
                result = fd_last.read()
            
        for tmpfile in tempfiles:
            if tmpfile != result:
                os.unlink(tmpfile)
            
        return result

    def bmass_intersect_quick(self, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, return_filename=False):
        return self.bmulticommand(COMMAND_INTERSECT, inputfsts, prune_weight, prune_level, detmin=detmin, sort=SORT_FIRST
                                  , return_filename=return_filename)

    def bmass_intersect_quick_pdt(self, inputfsts, prune_weight = -1, prune_level = 0, detmin=True, return_filename=False):
        return self.bmulticommand(COMMAND_PDTINTERSECT, inputfsts, prune_weight, prune_level, detmin=detmin, sort=SORT_NONE
                                  , return_filename=return_filename)
        
    def mass_intersect_quick(self, inputfsts, intersect_filename, prune_weight = -1, prune_level = 0, detmin=True):
        """ intersects a number of acceptors """
        
        filename = self.bmulticommand(COMMAND_INTERSECT, inputfsts, prune_weight, prune_level, 
                                      detmin=detmin, sort=SORT_FIRST, return_filename=True )
        shutil.move(filename, "%s/%s.bin" % (self.workingDir, intersect_filename))
        
        
    def write_sequence_logo(self, count_matrix, alphabet, fout, name="Profile Logo"):
        import weblogolib

        """ creates a sequence logo """
        data = weblogolib.LogoData.from_counts("".join(alphabet), count_matrix)
        options = weblogolib.LogoOptions()
        options.logo_title = name
        options.show_fineprint = False
        cs = weblogolib.ColorScheme([weblogolib.ColorGroup("0", "#4C00FF"),
                                     weblogolib.ColorGroup("1", "#0F00FF"),
                                     weblogolib.ColorGroup("2", "#002EFF"),
                                     weblogolib.ColorGroup("3", "#006BFF"),
                                     weblogolib.ColorGroup("4", "#00A8FF"),
                                     weblogolib.ColorGroup("5", "#00E5FF"),
                                     weblogolib.ColorGroup("6", "#00FF4D"),
                                     weblogolib.ColorGroup("7", "#00FF00"),
                                     weblogolib.ColorGroup("8", "#4DFF00"),
                                     weblogolib.ColorGroup("9", "#99FF00"),
                                     weblogolib.ColorGroup("a", "#E6FF00"),
                                     weblogolib.ColorGroup("b", "#FFFF00"),
                                     weblogolib.ColorGroup("c", "#FFEA2D"),
                                     weblogolib.ColorGroup("d", "#FFDE59"),
                                     weblogolib.ColorGroup("e", "#FFDB86"),
                                     weblogolib.ColorGroup("f", "#FFE0B3"),
                                     weblogolib.ColorGroup("X","red"),
                                     ])
        options.color_scheme = cs
        format = weblogolib.LogoFormat(data, options)
        weblogolib.pdf_formatter( data, format, fout)

        
    def sequences_to_fsas(self, input_file, input_format, to_lower=False):
        """ Takes the name of a sequence file and its format and turns the sequences into textual FSTs
        that are put into the output directory.
        Sequences must have unique names. """
        
        sequence_files = []
        
        self.log.write("Creating FSAs from FASTA file...")
        with open(input_file, "r") as fd_fasta:
            sequences = [record for record in SeqIO.parse(fd_fasta, input_format)]
            ids = [s.id for s in sequences] 
            unique_ids = list(set(ids))
            
            if len(ids) != len(unique_ids):
                self.log.writeln("Not all sequence ids are unique!")
                sys.exit(1)
            
            for seq in sequences:
                filename = seq.id.replace("/", "_") ## filenames mustn't contain "/"
                fsa = str(FSTFactory.create_fixed_length_hmm_from_sequences([seq,], self.config.alphabet, 
                            pseudo_counts = 0, semiring = Fst.Semiring.LOG, to_lower = to_lower))
        
                output_path = "%s/%s.fst" % (self.workingDir, filename)
                if os.path.exists(output_path):
                    self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
                
                sequence_files.append(filename)
                
                with open(output_path,"w") as fd_out:
                    fd_out.write(fsa)
        self.log.writeln("done!")
        
        return sequence_files

    def sequences_to_fsas_multiple(self, input_files, input_format, to_lower=False):
        """ Takes the name of a sequence file and its format and turns the sequences into textual FSTs
        that are put into the output directory.
        Sequences do not need to have unique names. """
        
        sequence_files = []
        
        self.log.write("Creating FSAs from FASTA file...")
        sequences = []
        for file in input_files:
            with open(file, "r") as fd_fasta:
                sequences += [record for record in SeqIO.parse(fd_fasta, input_format)]
                
        ids = [s.id for s in sequences] 
        unique_ids = list(set(ids))
        
        if len(ids) != len(unique_ids):
            self.log.writeln("Not all sequence ids are unique!")
            #sys.exit(1)
        
        for id in unique_ids:
            filename = id.replace("/", "_") ## filenames mustn't contain "/"
            selected_sequences = filter(lambda x:x.id==id, sequences)
            fsa = str(FSTFactory.create_fixed_length_hmm_from_sequences(selected_sequences, self.config.alphabet, 
                        pseudo_counts = 0, semiring = Fst.Semiring.LOG, to_lower = to_lower))
    
            output_path = "%s/%s.fst" % (self.workingDir, filename)
            if os.path.exists(output_path):
                self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
            
            sequence_files.append(filename)
            
            with open(output_path,"w") as fd_out:
                fd_out.write(fsa)
        self.log.writeln("done!")
        
        return sequence_files

    def compile_sequences(self, sequence_files, acceptors=True):
        """ compiles the textual fsts sequence_files in the directory working_dir """
        
        self.log.write("Compiling the FSAs...")
        
        for filename in sequence_files:
            try:
                subprocess.check_call(shlex.split("fstcompile --isymbols=%s --osymbols=%s --keep_isymbols\
                --keep_osymbols %s %s/%s.fst %s/%s.bin"
                           % (self.config.symbol_file, 
                              self.config.symbol_file,
                              "--acceptor" if acceptors else "", 
                              self.workingDir, 
                              filename, 
                              self.workingDir, filename), posix=not IS_WINDOWS), shell=platform.system()=="Windows")
            except CalledProcessError as cpe:
                raise CnvToolsError(cpe.errmsg)
        self.log.writeln("done!")

    def encode_sequence(self, filename, codex, reuse_codex=False, decode=False, encode_labels=True, encode_weights=False):
        """ compiles the textual fsts sequence_files in the directory working_dir """
        
        self.log.write("Encoding " + filename + "...")
        if not decode:
            encode_filename = filename + "_enc"
        else:
            encode_filename = filename + "_dec"
        
        try:
            subprocess.check_call(shlex.split("fstencode %s %s %s %s %s/%s.bin %s %s/%s.bin"
                       % ("--encode_labels" if encode_labels and not decode else "", 
                          "--encode_weights" if encode_weights and not decode else "",
                          "--encode_reuse" if reuse_codex and not decode else "",
                          "--decode" if decode else "",                          
                          self.workingDir, 
                          filename,
                          codex,
                          self.workingDir, 
                          encode_filename), posix=not IS_WINDOWS), shell=platform.system()=="Windows")
        except CalledProcessError as cpe:
            raise CnvToolsError(cpe.errmsg)
        self.log.writeln("done!")
        return encode_filename

    def bencode(self, fst, codex, reuse_codex=False, decode=False, encode_labels=True, encode_weights=False):
        """ compiles the textual fsts sequence_files in the directory working_dir """
        
        self.log.write("Encoding..." if not decode else "Decoding...")
        p = Popen(shlex.split("fstencode %s %s %s %s - %s"
                       % ("--encode_labels" if encode_labels and not decode else "", 
                          "--encode_weights" if encode_weights and not decode else "",
                          "--encode_reuse" if reuse_codex and not decode else "",
                          "--decode" if decode else "",                          
                          codex), posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=fst)
        _cleanup(p)

        if p.returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def bprune(self, inputfst, weight=-1, nstate=-1):
        """ prunes a fst, expects fst as binary and returns binary """
        
        self.log.write("Pruning...")
        p=[]
        p.append(Popen(shlex.split("fstprune --weight=%s --nstate=%d" % 
                                   (str(weight) if weight>=0 else "", nstate), posix=not IS_WINDOWS), 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                                    close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def brandgen(self, inputfst, select="uniform", npath=1):
        """ generates random paths from a fst
        Selection type: one of:  "uniform", "log_prob" (when appropriate), "fast_log_prob" (when appropriate) """

        self.log.write("Sampling %d paths..." % npath)
        p=[]
        p.append(Popen(shlex.split("fstrandgen --select=%s --npath=%d" % 
                                   (select, npath), posix=not IS_WINDOWS), 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                                    close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def brandgenweighted(self, inputfst, npath=1):
        """ generates weighted random paths using log_prob selector from a fst """
        
        self.log.write("Sampling %d weighted paths..." % npath)
        p=[]
        p.append(Popen(shlex.split("%s %d" % 
                                   (RANDGENW_BINARY, npath), posix=not IS_WINDOWS), 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                                    close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])
                
    def bshortestpath(self, infst, nshortest=1, weight=""):
        """ prunes a fst, expects fst as binary and returns binary """
        
        self.log.write("Shortest path...")
        p=[]
        p.append(Popen(shlex.split("fstshortestpath %s --nshortest=%d" % 
                                   ("--weight=%d" % int(weight) if weight!="" and nshortest>1 else "", nshortest), posix=not IS_WINDOWS), 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(infst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def bshortestpath_pdt(self, infst, keep_parens=False):
        """ prunes a pdt, expects pdt as binary and returns binary """
        
        self.log.write("Shortest path...")
        p=[]
        p.append(Popen(shlex.split("pdtshortestpath %s --pdt_parentheses=%s" % ("--keep_parentheses" if keep_parens else "", self.config.pdt_parens_file),
                                   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(infst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def bcompose_pdt(self, infst1, infst2, left_pdt=True):
        """ composes a fst with a pdt, returns binary """
        
        self.log.write("Pdtcompose...")
        p=[]
        p.append(Popen(shlex.split("pdtcompose --pdt_parentheses=%s %s %s %s" % (self.config.pdt_parens_file, "--left_pdt=false" if not left_pdt else "", self._input_filename(infst1), self._input_filename(infst2)),
                                   posix=not IS_WINDOWS), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def bshortestdistance(self, infst, reverse=False):
        """ computes shortestdistance in a fst """
        
        self.log.write("Shortest distance...")
        p=[]
        p.append(Popen(shlex.split("fstshortestdistance --reverse=%s -" % str(reverse).lower(), posix=not IS_WINDOWS), 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                     close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(infst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            reader = csv.reader(StringIO(result[0]), delimiter="\t")
            shortest_distances = [(int(a1), float(a2)) for a1, a2 in reader]
            if not len(shortest_distances): 
                raise CnvToolsError("Shortest distance failed")
            return shortest_distances
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])
        
    def compose_sequences(self, fst1, fst2):
        """ composes two fsts """
        
        self.log.write("Composing " + fst1 + " with " + fst2 + "...")
        compose_name = fst1 + "_o_" + fst2
        
        p=[]
        p.append(Popen(shlex.split("fstarcsort --sort_type='olabel' %s/%s.bin" % (self.workingDir, fst1), posix=not IS_WINDOWS),
                                   stdout = subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append(Popen(shlex.split("fstcompose - %s/%s.bin %s/%s.bin" % 
                                   (self.workingDir, fst2,
                                    self.workingDir, compose_name), posix=not IS_WINDOWS),
                                   stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode==0:
            self.log.writeln("done!")
            return compose_name
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def project_sequence(self, filename, to_output=False, suffix=""):
        """ projects the fsts """
        
        self.log.write("Projecting " + filename + "...")
        if not to_output:
            project_filename = filename + ("_in" if suffix=="" else suffix)
        else:
            project_filename = filename + ("_out" if suffix=="" else suffix)
        
        try:
            subprocess.check_call(shlex.split("fstproject %s %s/%s.bin %s/%s.bin"
                       % ("--project_output" if to_output else "", 
                          self.workingDir, 
                          filename,
                          self.workingDir, 
                          project_filename), posix=not IS_WINDOWS), shell=platform.system()=="Windows")
        except CalledProcessError as cpe:
            raise CnvToolsError(cpe.errmsg)
        self.log.writeln("done!")
        return project_filename

    def bproject(self, inputfst, to_output=False):
        """ projects the fsts """
        
        self.log.write("Projecting...")
        
        p=[]
        p.append(Popen(shlex.split("fstproject %s"
                       % ("--project_output" if to_output else ""), posix=not IS_WINDOWS), 
                       stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                       close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def bcompile(self, textfst, acceptor=False):
        """ compiles the fst """
        
        self.log.write("Compiling...")
        
        p=[]
        p.append(Popen(shlex.split("fstcompile %s --isymbols=%s --osymbols=%s --keep_isymbols --keep_osymbols" % ("--acceptor" if acceptor else "", self.config.symbol_file, self.config.symbol_file), posix=platform.system()!="Windows"), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=str(textfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def barcsort(self, fst, sort_type="olabel"):
        """ arcsorts the fst """
        
        self.log.write("Sorting (arc)...")
        
        p=[]
        p.append(Popen(shlex.split("fstarcsort --sort_type=%s" % sort_type, posix=platform.system()!="Windows"), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(fst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])
                
    def bmap(self, inputfst, map_type="identity"):
        """ maps the fsts """
        
        self.log.write("Mapping (%s)..." % map_type)
        
        p=[]
        p.append(Popen(shlex.split("fstmap --map_type='%s'"
                       % map_type, posix=not IS_WINDOWS), 
                       stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def binvert(self, inputfst):
        """ inverts the fst """
        
        self.log.write("Inverting...")
        
        p=[]
        p.append(Popen("fstinvert", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        result = p[-1].communicate(input=self._input_binary(inputfst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def detmin_sequence(self, filename, determinize=True, minimize=True):
        """ determinizes and minimizes the fsas """
        
        self.log.write("Determinizing " + filename + "...")
        detmin_filename = filename + "_dm"
        try:
            p=[]
            if determinize: 
                p.append(Popen(shlex.split("fstdeterminize %s/%s.bin" % (self.workingDir, filename), posix=not IS_WINDOWS), 
                               stdout=subprocess.PIPE, shell=platform.system()=="Windows"))
            if minimize:
                p.append(Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), stdout=subprocess.PIPE, stdin=p[-1].stdout, shell=platform.system()=="Windows"))
            
            binary = p[-1].communicate()[0]
            _cleanup(p)
            with open("%s/%s.bin" % (self.workingDir, detmin_filename), "wb") as fd:
                fd.write(binary)
                
        except CalledProcessError as cpe:
            self.log.writeln("failed!")
            raise CnvToolsError(cpe.errmsg)
            
        self.log.writeln("done!")
        
        return detmin_filename

    def bdetmin(self, inputfst, rmepsilon=False, determinize=True, minimize=True):
        """ determinizes and minimizes the fsas """
        
        self.log.write("Determinizing...")
        p=[]
        
        if rmepsilon:
            p.append(Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS), 
                           stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        if determinize:
            p.append(Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS), 
                           stdout=subprocess.PIPE, stdin=p[-1].stdout if rmepsilon else subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        if minimize:
            p.append(Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), 
                           stdout=subprocess.PIPE, stdin=p[-1].stdout if determinize or rmepsilon else subprocess.PIPE, 
                           stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        
        p[0].stdin.write(self._input_binary(inputfst))
        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])
                    
    def bprint(self, infst):
        """ converts fst to text format """
        
        self.log.write("Printing...")
        p=[]
        p.append(Popen(shlex.split("fstprint", posix=not IS_WINDOWS), 
                       stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        
        result = p[-1].communicate(input=self._input_binary(infst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def binfo(self, infst):
        """ converts fst to text format """
        
        self.log.write("Analysing...")
        p=[]
        p.append(Popen(shlex.split("fstinfo", posix=not IS_WINDOWS), 
                       stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows"))
        
        result = p[-1].communicate(input=self._input_binary(infst))
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def identify_diploid(self, diploid):
        if not os.path.exists("%s/%s.bin" % (self.workingDir, diploid)):
            candidates = glob.glob("%s/*diploid*fst*" % self.workingDir)
            if len(candidates) == 0:
                raise CnvToolsError("No diploid found and nothing looked like one (filename should contain 'diploid')!")
            else:
                self.log.writeln("No diploid found, but %s looks like one!" % candidates[0])
                return os.path.basename(candidates[0]).split(".")[0] ## first part of filename before dot and without path
        else:
            self.log.writeln("Diploid is as defined: %s" % diploid)
            return diploid

    def run_path_depth_first_search(self, infst):
        reader = fst.io.OpenFstTextReader(StringIO(self.bprint(infst)))
        myfst = reader.create_fst()
        algo = fst.algos.PathDepthFirstSearchNew(myfst)
        return algo


    def get_sequences(self, infst):
        algo = self.run_path_depth_first_search(infst)
            
        paths=list()
        for path in algo.get_paths():
            seq = "".join([p.symbol_from for p in path])
            paths.append(seq)

        return paths
        
    def _input_binary(self, param):
        """ either reads file if file handle or returns input. In any case the output
        is a binary transducer or None """
        result = None
        
        if isinstance(param, file) and not param.closed:
            result = param.read()
        else:
            try:
                if os.path.exists(param): ## is absolute or relative path
                    with open(param, "rb") as fd:
                        result = fd.read()
                else: ## could be a filename
                    path = "%s/%s.bin" % (self.workingDir, param)
                    if os.path.exists(path): ## let's see if it exists now...
                        with open(path, "rb") as fd:
                            result = fd.read()
            except TypeError: ## probably a binary fst
                result = param
            
        return result

    def _input_filename(self, param):
        """ either writes file if param is binary or returns input if filename. In any case the output
        is a filename. It's in the responsibility of the caller to delete it if necessary."""
        
        filename = None
        if isinstance(param, file):
            filename = param.name
        else:
            try:
                if os.path.exists(param): ## is absolute or relative path
                    filename = param
                else: ## could be a filename
                    path = "%s/%s.bin" % (self.workingDir, param)
                    if os.path.exists(path): ## let's see if it exists now...
                        filename = path
            except TypeError: ## probably a binary fst
                with tempfile.NamedTemporaryFile(delete=False) as fd_tmp:                
                    fd_tmp.write(param)
                    filename = fd_tmp.name
        
        return filename
        
    def write_tree_files(self, tree, filename, output_dir=None, write_newick=True, 
                         write_nexus=False, write_xml=True, write_drawing=True, write_picture=True):
        outdir = output_dir if output_dir else self.workingDir
        if write_xml:
            with open("%s/%s.xml" % ( outdir , filename), "w") as fd:
                Phylo.write(tree, fd, "phyloxml")

        if write_newick:
            with open("%s/%s.new" % ( outdir, filename), "w") as fd:
                Phylo.write(tree, fd, "newick", branch_length_only=True)
        
        if write_nexus:
            with open("%s/%s.nex" % ( outdir, filename), "w") as fd:
                Phylo.write(tree, fd, "nexus")
        
        if write_drawing:            
            drawing = StringIO()
            Phylo.draw_ascii(tree, file = drawing)
            with open("%s/%s.graph" % (outdir, filename), "w") as fd:
                fd.write(drawing.getvalue())
            drawing.close()
            
        #        import pylab
        #        Phylo.draw_graphviz(tree)
        #        pylab.show()
        #        pylab.savefig('apaf.png')

    def sum_sequences(self, fst1, fst2):
        """ sums the CNs of two sequences
        can be filenames, or binary data. """
        
        fstobj1 = fst.io.OpenFstTextReader(StringIO(self.bprint(fst1))).create_fst()
        fstobj2 = fst.io.OpenFstTextReader(StringIO(self.bprint(fst2))).create_fst()
        
        path1 = fst.algos.PathDepthFirstSearchNew(fstobj1).get_paths().next()
        path2 = fst.algos.PathDepthFirstSearchNew(fstobj2).get_paths().next()
        
        textsequence1 = [t.symbol_from for t in path1]
        textsequence2 = [t.symbol_from for t in path2]
        
        result = self.sum_text_sequences(textsequence1, textsequence2)

        fstobj_sum = fst.factory.FSTFactory.create_fixed_length_hmm_from_sequences([result,], self.config.alphabet, pseudo_counts=0) 
        fstobj_bin = self.bcompile(fstobj_sum, acceptor=True)
        
        return fstobj_bin
        
    def sum_text_sequences(self, textsequence1, textsequence2):
        """ sums two textsequences """
        
        maxval = max([int(s, 16) for s in self.config.alphabet])
        retval = list()
        for s1, s2 in zip(textsequence1, textsequence2):
            try:
                val1 = int(s1, 16)
                val2 = int(s2, 16)
                result = hex(min( val1 + val2, maxval ))[2:]
            except ValueError:
                if s1 == s2:
                    result = s1
                else:
                    raise CnvToolsError("Error adding sequences: %s and %s not \
                          identical" % (s1, s2))
            retval.append(result)
                
        #result = [hex(min( int(s1, 16) + int(s2, 16), maxval ))[2:] for s1, s2 in zip(textsequence1, textsequence2)]
        
        return retval
            

class CNVAligner(object):
    """ Toolbox next version for doing lots of basic things with CNV FSTs """
    
    def __init__(self, workingDir, configuration, path_to_alignment_fst, logger=None):
        """ constructor """
        if not isinstance(configuration, CnvBaseConfig):
            raise CnvToolsError("Need configuration object of class CnvBaseConfig")
            
        self.config = configuration
        self.fstpath = path_to_alignment_fst
        if logger==None:
            self.log = Log(is_logging=True)
        else:
            self.log = logger
        self.cnvtools = CnvToolBox(workingDir, configuration, logger=self.log)
        self.workingDir = workingDir
    
    def project(self, filename, detweight=-1, minimize = True, lazy = False, from_right=False, project=True, rmweights=False):
        """ composes the given sequence with the aligner and projects to output (unless project=false)"""
        
        self.log.write("Composing " + filename + " with aligner " + self.fstpath + "...")
        if not os.path.isabs(filename):
            composed_filename = self.workingDir + "/" + filename + "_edit.bin"
        else:
            composed_filename = filename + "_edit.bin"
        
        if not lazy or not os.path.exists(self.workingDir + "/" + composed_filename + ".bin"):
            p = []  
            if not from_right: ## build SEQ o ED, project to output
                p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" 
                                            % self.cnvtools._input_filename(filename), posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                            stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                p.append( Popen(shlex.split("fstcompose - %s" 
                                            % self.fstpath, posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
                                            stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                if project:
                    p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), 
                                stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
            else: ## build ED o SEQ, project to input
                p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' %s" 
                                            % self.cnvtools._input_filename(filename), posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                            stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                p.append( Popen(shlex.split("fstcompose %s -" 
                                            % self.fstpath, posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
                                            stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                if project:
                    p.append( Popen(shlex.split("fstproject", posix=not IS_WINDOWS), 
                                    stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

            if minimize and project: ## can't minimize if not projecting
                p.append( Popen(shlex.split("fstdeterminize" + (" --weight=%f" % (detweight) if detweight >=0 else ""), posix=not IS_WINDOWS)
                                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                if detweight>=0:
                    self.log.write("detweight=%f..." % detweight)
                p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
                                stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                
            if rmweights:
                p.append( Popen(shlex.split("fstmap --map_type='rmweight'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
                                stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
                
                
            result = p[-1].communicate()
            _cleanup(p)
            if p[-1].returncode==0:
                with open(composed_filename, "wb") as fd:
                    fd.write(result[0])
                self.log.writeln("done!")
            else:
                self.log.writeln("failed!")
                raise CnvToolsError(result[1])

        else:
            self.log.writeln("skipped!")
        
        return composed_filename

    def projectPDT(self, filename, detweight=-1, minimize = True, from_right=False, project=True, rmweights=False):
        """ composes the given sequence with the aligner and projects to output (unless project=false) """
        
        self.log.write("Composing " + filename + " with aligner " + self.fstpath + "...")
        if not os.path.isabs(filename):
            composed_filename = self.workingDir + "/" + filename + "_edit.bin"
        else:
            composed_filename = filename + "_edit.bin"
        
        p = []

        if not from_right: ## build SEQ o ED, project to output
            p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' %s" 
										% self.fstpath, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
            p.append( Popen(shlex.split("pdtcompose --left_pdt=true --pdt_parentheses=%s %s -" 
										% (self.config.pdt_parens_file, self.cnvtools._input_filename(filename)), posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
            if project:
                p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), 
							stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

        else: ## build ED o SEQ, project to input
            p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" 
										% self.fstpath, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
            p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" 
										% (self.config.pdt_parens_file, self.cnvtools._input_filename(filename)), posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
            if project:
                p.append( Popen(shlex.split("fstproject", posix=not IS_WINDOWS), 
								stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

        if minimize and project: ## can't minimize if not projecting
            p.append( Popen(shlex.split("fstdeterminize" + (" --weight=%f" % (detweight) if detweight >=0 else ""), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
            if detweight>=0:
                self.log.write("detweight=%f..." % detweight)
            p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
        
        if rmweights:
            p.append( Popen(shlex.split("fstmap --map_type='rmweight'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )


        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode==0:
            with open(composed_filename, "wb") as fd:
                fd.write(result[0])
            self.log.writeln("done!")
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

        return composed_filename

    def align_pairwise(self, filenames):
        """ computes pairwise distances between the files in filenames and returns the result as numpy array. """
        
        distances = numpy.zeros((len(filenames), len(filenames)))
        
        self.log.write("Computing pairwise distances...")
        for i in range(0, len(filenames)):
            for j in range(i+1, len(filenames)):
                f1 = self.cnvtools._input_filename(filenames[i])
                f2 = self.cnvtools._input_filename(filenames[j])
                p1 = Popen(shlex.split("%s %s %s %s" % 
                                       (ALIGN_BINARY, 
                                        self.fstpath, 
                                        f1, f2), posix=not IS_WINDOWS)
                                       , stdout=subprocess.PIPE, stderr = subprocess.PIPE)
                result = p1.communicate()
                _cleanup(p1)
                if p1.returncode!=0:
                    self.log.writeln("failed!")
                    raise CnvToolsError(result[1])
                    
                distance = float(result[0])
                
                
                distances[i,j] = distance
                distances[j,i] = distance
    
        self.log.writeln("done!")
        return distances

    def align_pairwise_pdt(self, filenames):
        """ computes pairwise distances between the files in filenames and returns the result as numpy array. 
        Assumes the sequence files are all PDTs. """
        
        distances = numpy.zeros((len(filenames), len(filenames)))
        
        self.log.write("Computing pairwise distances from PDTs...")
        for i in range(0, len(filenames)):
            for j in range(i+1, len(filenames)):
                f1 = self.cnvtools._input_filename(filenames[i])
                f2 = self.cnvtools._input_filename(filenames[j])
                p=[]
                p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" %  f1, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("pdtcompose --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, self.fstpath), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("pdtexpand --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("fstarcsort --sort_type='olabel'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, f2), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("pdtshortestpath --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                p.append( Popen(shlex.split("fstshortestdistance --reverse=true", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
                result = p[-1].communicate()
                _cleanup(p)
                if p[-1].returncode!=0:
                    self.log.writeln("failed!")
                    raise CnvToolsError(result[1])
                    
                distance = float(StringIO(result[0]).next().strip("\n").split("\t")[1])
                
                distances[i,j] = distance
                distances[j,i] = distance
    
        self.log.writeln("done!")
        return distances
    
    def find_closest_target_pdt(self, source, target):
        """ finds the closest target sequence from source using the given transducer.
       Target is a PDT. """
        
        value = -1
        self.log.write("Computing new distances...")
        p=[]
        p.append( Popen(shlex.split("fstcompose %s %s" %  (self.cnvtools._input_filename(source), self.fstpath), posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, self.cnvtools._input_filename(target)), posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("pdtshortestpath --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("pdtexpand --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS)
                            , stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
        
        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def align_pair(self, source, target, shortest_path = False):
        """ computes shortest distance (or shortest path if shortest_path=True) from the source file to the target file """
        
        value = -1
        self.log.write("Computing new distances...")
        p1 = Popen(shlex.split("%s %s %s %s" % 
                               (SP_BINARY if shortest_path else ALIGN_BINARY, 
                                self.fstpath, 
                                self.cnvtools._input_filename(source), 
                                self.cnvtools._input_filename(target)), posix=not IS_WINDOWS)
                               , stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell = IS_WINDOWS)
        
        result = p1.communicate()
        _cleanup(p1)
        if p1.returncode == 0:
            self.log.writeln("done!")
            if shortest_path:
                return result[0]
            else:
                try:
                    value = float(result[0])
                except ValueError:
                    value = -1
                    self.log.writeln("WARNING: No result returned when aligning %s to %s!" % (source, target))
                return value
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])

    def balign_pair_full(self, source, target):
        """ computes full composition of source o aligner o target """
        
        self.log.write("Composing with aligner...")
        p=[]
        p.append(Popen(shlex.split("fstarcsort --sort_type='olabel' %s/%s.bin" % (self.workingDir, source), posix=not IS_WINDOWS), 
                   stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append(Popen(shlex.split("fstcompose - %s" % self.fstpath, posix=not IS_WINDOWS), 
                   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append(Popen(shlex.split("fstarcsort --sort_type='olabel'", posix=not IS_WINDOWS), 
                   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
        p.append(Popen(shlex.split("fstcompose - %s/%s.bin" % (self.workingDir, target), posix=not IS_WINDOWS), 
                   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
        
        result = p[-1].communicate()
        _cleanup(p)
        if p[-1].returncode == 0:
            self.log.writeln("done!")
            return result[0]
        else:
            self.log.writeln("failed!")
            raise CnvToolsError(result[1])


        
class CnvToolsError(CnvPhyloError):
    """ Exception class for this module """ 
    def __init__(self, message):
        super(CnvToolsError, self).__init__(message)


def _cleanup(p):
    if isinstance(p, subprocess.Popen):
        p = [p,]
    for q in p:
        if not q.stdin == None:
            q.stdin.close()
        if not q.stdout == None:
            q.stdout.close()
        if not q.stderr == None:
            q.stderr.close()
