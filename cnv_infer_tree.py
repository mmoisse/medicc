#!/usr/bin/python

'''
Created on 25 Nov 2010

@author: rfs
'''

import sys
from optparse import OptionParser
from commons.tools import log
import os
import numpy
import scipy
import scipy.optimize
from Bio import Phylo
import Bio.SeqIO
#import glob
import cnvphylo.cnvtools
import cnvphylo.trees
import subprocess
from subprocess import Popen 
import shlex
from cnvphylo.dualtree import *



#CNV_ALPHABET = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')

#DISTANCE_FILENAME = "tree.dist"
#C_EVENT_LEN_PROB = 0.2


LOG = log.Log()

OPT = None

        
    
def main():
    """ the main method """

    usage = "USAGE: %prog <major_cn_file> <minor_cn_file> <output_dir>"    
    parser = DualTreeFactory.create_option_parser(usage)

    (options, args) = parser.parse_args()
   
    if len(args) != 3:
        parser.error("incorrect number of arguments")
    
    LOG.is_logging = options.verbose
    
    major_cn_file = args[0]
    minor_cn_file = args[1]
    output_dir = args[2]
    
    if not os.path.exists(major_cn_file):
        LOG.writeln("Major CN file does not exist!")
        sys.exit(1)

    if not os.path.exists(minor_cn_file):
        LOG.writeln("Minor CN file does not exist!")
        sys.exit(1)
    
    app = DualTreeApp(major_cn_file, minor_cn_file, output_dir, options)
    app.run()



if __name__ == '__main__':
    sys.exit(main())
   

