#!/usr/bin/python

""" simulates CNV evolution """

import sys
from optparse import OptionParser
from fst.fst import *
from fst.factory import FSTFactory
from fst.io import *
import shlex, subprocess
from subprocess import CalledProcessError
from subprocess import Popen
from commons.tools import log
import os
import numpy
import Bio
import Bio.Phylo
import glob
from StringIO import StringIO

import cnvphylo.cnvtools
from cnvphylo import cnvtools, CnvBaseConfig

LOG = log.Log()
DIPLOID_NAME = "diploid"
CNV_ALPHABET = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')
RES_TREE_FILE = "random"
RES_TREE_GRAPH = "random.txt"
RES_SEQ_FILE_TERM = "random.fasta"
RES_SEQ_FILE_NONTERM = "random_nt.fasta"
RES_DIST_FILE = "random.dist"
RES_STATS_FILE = "random.stats"
CHR_PREFIX = "chr"
CHR_SEPARATOR = "X"
 
def main():
    """ main method """
    
    usage = "USAGE: %prog <output_dir>"
    
    parser = OptionParser(usage)
    
    parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
                      default = False, help = "be verbose?")
    parser.add_option("-t", "--taxa", action = "store", type="int", dest = "ntaxa",
                      default = 10, help = "Number of taxa to create")
    parser.add_option("-m", "--min-branch-length", action = "store", type="int", dest = "min_blength",
                      default = 1, help = "Minimum branch length")
    parser.add_option("-M", "--max-branch-length", action = "store", type="int", dest = "max_blength",
                      default = 10, help = "Maximum branch length")
    parser.add_option("-d", "--no-diploid", action = "store_false", dest = "diploid",
                      default = True, help = "do not add diploid normal?")
    parser.add_option("-l", "--length", action = "store", type="int", dest = "seq_length",
                      default = 200, help = "Chromosome/profile length")
    parser.add_option("-p", "--prob-amp", action = "store", type="float", dest = "amp_prob",
                      default = 0.7, help = "amplification probability")
    parser.add_option("-q", "--end-amp", action = "store", type="float", dest = "end_prob",
                      default = 0.2, help = "event end probability for geometric distribution")
    parser.add_option("-c", "--number-of-chromosomes", action = "store", type="int", dest = "nchr",
                      default = 1, help = "number of chromosomes (default 1)")
    parser.add_option("-u", "--upper-bound", action = "store", type="int", dest = "upper_bound",
                      default = 15, help = "upper bound for amplification level (default 15)")
    parser.add_option("-s", "--split-chromosomes", action="store_true", default = False, 
                      help = "split chromosomes into separate files?", dest = "split")
    parser.add_option("", "--prefix", action = "store", type="string", dest = "prefix",
                      default = "", help = "file prefix")
    parser.add_option("", "--tree", action = "store", type="string", dest = "tree",
                      default = "", help = "use existing tree file (absolute or relative to output_dir)")
    parser.add_option("", "--keep-branchlengths", action = "store_true", dest = "keep_blengths",
                      default = False, help = "if tree file specified, keep branch lengths in the tree or assign new ones? ")
    parser.add_option("", "--tree-file-format", action = "store", type="string", dest = "tree_file_format",
                      default = "phyloxml", help = "format of tree file, one of {nexus, newick, phyloxml} (default phyloxml)")

    
#    parser.add_option("-n", "--samples", action = "store", type="int", dest = "nsamples",
#                      default = 40, help = "Number of samples to generate")
    
    
    (options, args) = parser.parse_args()
    
    if len(args) != 1:
        parser.error("incorrect number of arguments")
        return 1
    else:
        outdir = args[0]
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        os.chdir(outdir)
    
    if not 0 <= options.amp_prob <= 1:
        parser.error("Amplification probability needs to be between 0 and 1")
        return 1
    
    if not 0 <= options.end_prob <= 1:
        parser.error("Event end probability needs to be between 0 and 1")
        return 1
    
        
    LOG.is_logging = options.verbose
    
    cnvtools = cnvphylo.cnvtools.CnvToolKit(CnvBaseConfig(".", None, options.verbose, None))
    
    if options.tree == "":    
        tree = random_tree(options.ntaxa, options.min_blength, options.max_blength, options.diploid)
    else:
        with open(options.tree, "r") as fd_in:
            tree = Bio.Phylo.read(fd_in, options.tree_file_format)
            if not options.keep_blengths:
                randomize_branch_lengths(tree, options.min_blength, options.max_blength)
    
    names, distmat = to_distance_matrix(tree)
    phylip = cnvtools.to_phylip_format(distmat, names, sort=True)
    with open(options.prefix + RES_DIST_FILE, "w") as fd:
        fd.writelines(phylip)
    
    if options.diploid:
        diploid = [c for c in tree.find_clades(name = DIPLOID_NAME)][0]
        root_with_outgroup(tree, diploid)

    if options.nchr < 1:
        sys.exit()
        
    if options.diploid:
        precursor = ["1", ] * options.seq_length
        for i in range(1, options.nchr):
            precursor += CHR_SEPARATOR
            precursor = precursor + ["1", ] * options.seq_length
    else:
        precursor = [CNV_ALPHABET[i] for i in numpy.random.randint(0, options.seq_length)]
        for i in range(1, options.nchr):
            precursor = precursor + [CNV_ALPHABET[i] for i in numpy.random.randint(0, options.seq_length)]
        
    sequences = evolve_along_tree(tree, precursor, options.amp_prob, options.end_prob, options.upper_bound)
    
    
    
    ## write files
    cnvtools.write_tree_files(tree, options.prefix + RES_TREE_FILE)

    with open(options.prefix + RES_SEQ_FILE_TERM, "w") as fd:
        fd.write(to_fasta(sequences, True))

    with open(options.prefix + RES_SEQ_FILE_NONTERM, "w") as fd:
        fd.write(to_fasta(sequences, False))

    with open(options.prefix + RES_STATS_FILE, "w") as fd:
        fd.writelines(format_stats(sequences))
    
    ## split chromosomes and write individual files
    splitted_sequences = split_sequences(sequences)
    count = 0
    
    names_term=list()
    names_nonterm=list()
    labels=list()
    
    for chr in splitted_sequences:
        count += 1
        label = "%s%.2d" % (CHR_PREFIX, count)
        name_term = "%s%s%.2d_%s" % (options.prefix, CHR_PREFIX, count, RES_SEQ_FILE_TERM)
        with open(name_term, "w") as fd:
            fd.write(to_fasta(chr, True))
        
        name_nonterm = "%s%s%.2d_%s" % (options.prefix, CHR_PREFIX, count, RES_SEQ_FILE_NONTERM)
        with open(name_nonterm, "w") as fd:
            fd.write(to_fasta(chr, False))
        
        labels.append(label + "\n")
        names_term.append(name_term + "\n")
        names_nonterm.append(name_nonterm + "\n")
        
    with open(options.prefix + "labels.txt", "w") as fd:
        fd.writelines(labels)
    with open(options.prefix + "filenames_term.txt", "w") as fd:
        fd.writelines(names_term)
    with open(options.prefix + "filenames_nonterm.txt", "w") as fd:
        fd.writelines(names_nonterm)
        
    internal_node_list = tree.get_nonterminals()
    internal_file = []
    for node in internal_node_list:
        children_names = ["%s (%f)" % (c.name, c.branch_length) for c in node.clades]
        line = " - ".join(children_names)
        line += ": "
        line += node.name if node.name else "unnamed"
        internal_file.append(line)

    with open(options.prefix + "internal_filenames.txt", "w") as fd:
        fd.write("\n".join(internal_file)+"\n")

def split_sequences(sequences):
    """ splits seqs at chromosome boundaries """
    
    result = list()
    splitted = list()
    labels = sorted(sequences.keys())
    
    for label in labels:
        s = "".join(sequences[label][0])
        s = s.split(CHR_SEPARATOR)
        s = [[c for c in w] for w in s]
        splitted.append((s, sequences[label][1]))
    
    nchr = len(splitted[0][0])
    for i in range(0, nchr):
        chr = dict()
        for label in labels:
            idx = labels.index(label)
            chr[label] =  (splitted[idx][0][i], splitted[idx][1])
        result.append(chr)
        
    return result
    
def format_stats(sequences):
    lines = []
    lines.append("name length events adj_bps\n")
    for seq in sequences:
        lines.append("%s %d %d %d\n" % (seq, len(sequences[seq][0]), sequences[seq][3], sequences[seq][2]))
    return lines
    
    
def to_distance_matrix(tree):
    """Create a distance matrix (NumPy array) from terminals in tree """
    
    terminals = tree.get_terminals()
    names = [t.name for t in terminals]
    distmat = numpy.zeros((len(terminals), len(terminals)))
    for i in terminals:
        for j in terminals:
            distmat[terminals.index(i), terminals.index(j)] =  tree.distance(i, j)
    return (names, distmat)

#def to_phylip_format(distances, filenames):
#    """ converts distance matrix and the names of the rows/columns into phylip compatible string """
#    lines = []
#    lines.append("%d\n" % len(filenames))
#    for i in range(0, distances.shape[0]):
#        line = "%-9s " % filenames[i]
#        line += " ".join(["%f" % val for val in distances[i, ]])
#        line += "\n"
#        lines.append(line)
#    return lines

def to_fasta(sequences, terminal):
    """ turns the sequences dictionary into a fasta formatted string """
    text = "\n".join([">"+ k + "\n" + "".join(sequences[k][0]) for k in sequences if sequences[k][1] == terminal])
    text += "\n"
    return text
        
def evolve_along_tree(tree, precursor, amp_prob, end_prob, upper_bound):
    """ Evolves sequences along a given tree starting from precursor """
    sequences = {}
    
    if tree.root.name:
        sequences[tree.root.name] = (precursor, tree.root.is_terminal())
    
    for clade in tree.root.clades:
        _evolve(clade, precursor, sequences, amp_prob, end_prob, upper_bound)
    
    return sequences

def _evolve(node, profile, sequences, amp_prob, end_prob, upper_bound):
    """ internal recursive method for evolving the sequence """
    
    (mutated, adj_bp_count) = mutate_sequence(profile, int(node.branch_length) , amp_prob, end_prob, upper_bound)
    #LOG.writeln("Adjacent bp count (%d mutations on sequence of length %d): %d" % (node.branch_length, len(profile), adj_bp_count))
    sequences[node.name] = (mutated, node.is_terminal(), adj_bp_count, int(node.branch_length))

    rec = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(data=".", alphabet=None),
           id=node.name, name=node.name,
           description="".join(mutated))
    
    node.sequences.append(Bio.Phylo.PhyloXML.Sequence.from_seqrecord(rec)) 
    
    for clade in node.clades:
        _evolve(clade, mutated, sequences, amp_prob, end_prob, upper_bound)

    
    
def root_with_outgroup(tree, *outgroup_targets):
    """Reroot this tree with the outgroup clade containing outgroup_targets.

    Operates in-place.

    Edge cases:

     - If outgroup == tree.root, no change
     - If outgroup is terminal, create new bifurcating root node with a
       0-length branch to the outgroup
     - If outgroup is internal, use the given outgroup node as the new
       trifurcating root, keeping branches the same
     - If the original root was bifurcating, drop it from the tree,
       preserving total branch lengths
    """
    # This raises a ValueError if any target is not in this tree
    # Otherwise, common_ancestor guarantees outgroup is in this tree
    outgroup = tree.common_ancestor(*outgroup_targets)
    outgroup_path = tree.get_path(outgroup)
    if len(outgroup_path) == 0:
        # Outgroup is the current root -- no change
        return

    prev_blen = outgroup.branch_length
    if outgroup.is_terminal():
        # Create a new root with a 0-length branch to the outgroup
        outgroup.branch_length = 0.0
        new_root = tree.root.__class__(branch_length=0, clades=[outgroup])
        if len(outgroup_path) == 1: ## outgroup path contains only the outgroup
            tree.root.clades.remove(outgroup)
            tree.root.clades.append(new_root)
        else:
            outgroup_path[-2].clades.remove(outgroup)
            outgroup_path[-2].clades.append(new_root)
    else:
        # Use the given outgroup node as the new (trifurcating) root
        new_root = outgroup
        new_root.branch_length = None

    # Tracing the outgroup lineage backwards, reattach the subclades under a
    # new root clade. Reverse the branches directly above the outgroup in
    # the tree, but keep the descendants of those clades as they are.
    new_parent = new_root
    for parent in outgroup_path[-2::-1]:
        parent.clades.pop(parent.clades.index(new_parent))
        prev_blen, parent.branch_length = parent.branch_length, prev_blen
        new_parent.clades.insert(0, parent)
        new_parent = parent
    # Finally, handle the original root according to number of descendents
    old_root = tree.root
    old_root.clades.pop(old_root.clades.index(new_parent))
    if len(old_root) == 1:
        # Delete the old bifurcating root & add branch lengths
        ingroup = old_root.clades[0]
        if ingroup.branch_length:
            ingroup.branch_length += prev_blen
        else:
            ingroup.branch_length = prev_blen
        new_parent.clades.insert(0, ingroup)
        # ENH: If annotations are attached to old_root, do... something.
    else:
        # Keep the old trifurcating/polytomous root as an internal node
        old_root.branch_length = prev_blen
        new_parent.clades.insert(0, old_root)

    tree.root = new_root
    tree.rooted = True
    return

def random_tree(ntaxa, min_branch_length, max_branch_length, add_diploid):
    """ creates a random tree with uniformly distributed branch lengths between min and max """
    
    ## first setup the clades
    clades = []
    for i in range(0, ntaxa):
        blength = numpy.random.random_integers(min_branch_length, max_branch_length)
        clades.append(Bio.Phylo.PhyloXML.Clade(branch_length = blength, name = "taxon_%d" % i))
        
    if add_diploid:
        clades.append(Bio.Phylo.PhyloXML.Clade(branch_length = blength, name = DIPLOID_NAME))
    
    ## now find the pairings, i.e. the structure of the tree
    internal_count = 0
    while len(clades)>3:
        left = clades.pop(numpy.random.randint(0, len(clades)))
        right = clades.pop(numpy.random.randint(0, len(clades)))
        blength = numpy.random.random_integers(min_branch_length, max_branch_length)
        new_clade = Bio.Phylo.PhyloXML.Clade(branch_length = blength, 
                                       name = "internal_%d" % internal_count, 
                                       clades = [left, right])
        internal_count += 1
        clades.append(new_clade)
    
    ## finally
    tree = Bio.Phylo.PhyloXML.Phylogeny(root = Bio.Phylo.PhyloXML.Clade(branch_length = 0, name = "root", clades = clades))
    
    return tree

def randomize_branch_lengths(tree, min_branch_length, max_branch_length):
    """ keeps topology but assigns random branch lengths """
    for clade in tree.find_clades():
        blength = numpy.random.random_integers(min_branch_length, max_branch_length)
        clade.branch_length = blength
    
def mutate_sequence(sequence, nmutations, amp_prob, event_end_prob, upper_bound):
    """ mutates given sequence in nmutations steps using the specified event probabilities """
    
    mutated = sequence[:]
#    bp_start = numpy.repeat(False, len(mutated) + 1)
#    bp_end = numpy.repeat(False, len(mutated) + 1)
    adj_bp_count = 0
    
    LOG.writeln("Performing %d mutations on sequence: %s" % (nmutations, "".join(sequence)))
    for i in range(0, nmutations):
        start = numpy.random.randint(0, len(mutated))
        if mutated[start] == CHR_SEPARATOR : start += 1 # chromosome border
        length = numpy.random.geometric(event_end_prob)
        end = start + length
        if end > len(mutated):
            end = len(mutated)
        
        try: ## stop events at chromosome borders
            search = mutated[start:end].index(CHR_SEPARATOR)
            end = search
        except ValueError:
            pass
        
#        bp_start[start] = True
#        bp_end[end] = True
        is_amplification = numpy.random.binomial(1, amp_prob)
        def ampfun(x):
            ival = int( x, 16 )
            return hex( min( ival + 1, upper_bound ) if ival!=0 else 0 )[2:]
        
        def delfun(x):
            return hex( max( int( x, 16 ) - 1, 0  ) )[2:]
        
        if is_amplification:
            mutated[start:end] = map( ampfun, mutated[start:end] )
            LOG.writeln("\tAmplification from %d to %d" % (start, end))
        else:
            mutated[start:end] = map( delfun, mutated[start:end] )
            LOG.writeln("\tDeletion from %d to %d" % (start, end))
#    adj_bp_count = sum(bp_start & bp_end)
    LOG.writeln("Adjacent bp count (%d mutations on sequence of length %d): %d" % (nmutations, len(sequence), adj_bp_count))
    LOG.writeln("Done! Result: " + "".join(mutated))
#    return (mutated, adj_bp_count)
    return (mutated, -1)
    
############################################################################

if __name__ == "__main__":
    sys.exit(main())


