#!/bin/bash

create_cnv_fst.py -c m ed_asymm_nls_step1.fst -a 01234 -zx
fstmake.sh ed_asymm_nls_step1.fst cnv.symbols > ed_asymm_nls_step1.bin 
fst_multicmd.py -c fstcompose -r 4 ed_asymm_nls_step1.bin -ms > ed_asymm_nls.bin

create_cnv_fst.py -c l ed_asymm_ls_step1.fst -a 01234 -zx
fstmake.sh ed_asymm_ls_step1.fst cnv.symbols > ed_asymm_ls_step1.bin 
fst_multicmd.py -c fstcompose -r 4 ed_asymm_ls_step1.bin -s > ed_asymm_ls.bin

create_cnv_fst.py -c m ed_asymm_nls_step1_xt.fst -a 0123456789a -x
fstmake.sh ed_asymm_nls_step1_xt.fst cnv.symbols > ed_asymm_nls_step1_xt.bin 
fst_multicmd.py -c fstcompose -r 15 ed_asymm_nls_step1_xt.bin -ms > ed_asymm_nls_xt.bin

echo "inverting..."

fstinvert ed_asymm_nls.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls.bin > ed_symm_nls_large.bin
##fstencode_and_minimize.sh ed_symm_nls_large.bin > ed_symm_nls.bin
##fstprint ed_symm_nls.bin > ed_symm_nls.fst

cp ed_symm_nls_large.bin ed_symm_nls.bin

fstinvert ed_asymm_nls_xt.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls_xt.bin > ed_symm_nls_large_xt.bin
fstencode_and_minimize.sh ed_symm_nls_large_xt.bin > ed_symm_nls_xt.bin
##fstprint ed_symm_nls_xt.bin > ed_symm_nls_xt.fst

echo "done."
