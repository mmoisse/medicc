#!/usr/bin/python

import fst
import fst.io
import fst.factory
import fst.fst
from fst.fst import Fst
from fst.factory import FSTFactory

with open("ed_symm_nls.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls = reader.create_fst()

with open("ed_symm_nls_xt.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls_xt = reader.create_fst()

## duplications
def add_duplications(fst):
    alphabet=["0","1","2","3","4","5","6"]

    oldInitState = fst.get_initial_state()
    newInitState = fst.add_state()
    fst.add_transition((newInitState, oldInitState, Fst.GAP, Fst.GAP, 0))
    fst.set_initial_state(newInitState)

    state=fst.add_state()
    fst.add_transition((newInitState, state, Fst.GAP, Fst.GAP, 0))
    fst.add_transitions([(newInitState, state, s, t, 0) for s in alphabet for t in alphabet if FSTFactory._hex_is_double(s, t)]) ## event open transitions
    fst.add_transitions([(state, state, s, t, 0) for s in alphabet for t in alphabet if FSTFactory._hex_is_double(s, t)]) ## event extend transitions

    state=fst.add_state()
    fst.add_transition((newInitState, state, Fst.GAP, Fst.GAP, 0))
    fst.add_transitions([(newInitState, state, s, t, 0) for s in alphabet for t in alphabet if FSTFactory._hex_is_half(s, t)]) ## event open transitions
    fst.add_transitions([(state, state, s, t, 0) for s in alphabet for t in alphabet if FSTFactory._hex_is_half(s, t)]) ## event extend transitions

add_duplications(fst_nls)
add_duplications(fst_nls_xt)

with open("ed_symm_nls.fst","w") as fd:
    fd.write(str(fst_nls))

with open("ed_symm_nls_xt.fst","w") as fd:
    fd.write(str(fst_nls_xt))





