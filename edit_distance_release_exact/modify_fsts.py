#!/usr/bin/python

import fst
import fst.io
import fst.factory

with open("ed_asymm_ls.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_ls = reader.create_fst()

with open("ed_asymm_nls.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls = reader.create_fst()

with open("ed_asymm_nls_xt.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls_xt = reader.create_fst()


for state in fst_ls.states:
    fst_ls.add_transition((state.id, state.id, '0', '0', 0))


for state in fst_nls.states:
    fst_nls.add_transition((state.id, state.id, '0', '0', 0))

for state in fst_nls_xt.states:
    fst_nls_xt.add_transition((state.id, state.id, '0', '0', 0))

with open("ed_asymm_ls.fst","w") as fd:
    fd.write(str(fst_ls))

with open("ed_asymm_nls.fst","w") as fd:
    fd.write(str(fst_nls))

with open("ed_asymm_nls_xt.fst","w") as fd:
    fd.write(str(fst_nls_xt))




