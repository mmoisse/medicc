#!/bin/bash

## make xt fst
## create only symbol table
../create_cnv_fst.py -c m -a 0123456789a -zx -p pdtparens.txt -n 1024 -s cnv.symbols tmp.fst 
rm tmp.fst


../create_cnv_fst.py -c m ed_asymm_nls_step1_xt.fst -a 0123456789a -x
##fstmake.sh ed_asymm_nls_step1_xt.fst cnv.symbols > ed_asymm_nls_step1_xt.bin 
fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols ed_asymm_nls_step1_xt.fst > ed_asymm_nls_step1_xt.bin
fst_multicmd.py -c fstcompose -r 15 ed_asymm_nls_step1_xt.bin -ms > ed_asymm_nls_xt.bin

fstinvert ed_asymm_nls_xt.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls_xt.bin > ed_symm_nls_large_xt.bin
fstencode_and_minimize.sh ed_symm_nls_large_xt.bin > ed_symm_nls_xt_nosymbols.bin
fstsymbols --isymbols=cnv.symbols --osymbols=cnv.symbols ed_symm_nls_xt_nosymbols.bin > ed_symm_nls_xt.bin

## first create the irreversible FST for pairwise distances
../create_cnv_fst.py -c m -a 01234 -zx -p pdtparens.txt -n 1024 ed_asymm_nls_step1.fst 
##fstmake.sh ed_asymm_nls_step1.fst cnv.symbols > ed_asymm_nls_step1.bin 
fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols ed_asymm_nls_step1.fst > ed_asymm_nls_step1.bin

fst_multicmd.py -c fstcompose -r 4 ed_asymm_nls_step1.bin -s > ed_asymm_nls_pre.bin ## originally -m as well
fstencode --encode_labels ed_asymm_nls_pre.bin codex | fstdeterminize | fstminimize | fstencode --decode - codex > ed_asymm_nls_no_symbols.bin
fstsymbols --isymbols=cnv.symbols --osymbols=cnv.symbols ed_asymm_nls_no_symbols.bin > ed_asymm_nls.bin


fstinvert ed_asymm_nls.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls.bin > ed_symm_nls_large.bin
#fstencode_and_minimize.sh ed_symm_nls_large.bin > ed_symm_nls.bin
cp ed_symm_nls_large.bin ed_symm_nls.bin

# ../medicc_binaries/bin/productfst ed_symm_nls.bin > ed_symm_dual.bin

## now the non reversible fst with length scoring for ancestral reconstruction
../create_cnv_fst.py -c l ed_asymm_ls_step1.fst -a 01234 -z
fstmake.sh ed_asymm_ls_step1.fst cnv.symbols > ed_asymm_ls_step1.bin 
fst_multicmd.py -c fstcompose -r 4 ed_asymm_ls_step1.bin -s > ed_asymm_ls_large.bin

fstencode --encode_labels ed_asymm_ls_large.bin codex | fstdeterminize --delta=1 | fstminimize | fstencode --decode - codex | fstprint | fstcompile --keep_isymbols --keep_osymbols --isymbols=./cnv.symbols --osymbols=./cnv.symbols > ed_asymm_ls.bin



rm ed_asymm_nls_*
rm ed_asymm_ls_step1.*
rm ed_symm_nls_large.bin
rm ed_symm_nls_large_xt.bin
rm ed_symm_nls_xt_nosymbols.bin
rm ed_asymm_ls_large.bin


