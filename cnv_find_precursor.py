#!/usr/bin/python

'''
Created on Sep 5, 2011

@author: rfs
'''
import cnvphylo
import sys
from optparse import OptionParser
from commons.tools import log
import os
import numpy
import Bio
import Bio.Phylo
import Bio.SeqIO
import Bio.SeqRecord
import Bio.Seq
import Bio.Alphabet
from cnvphylo import cnvtools
import fst.factory

LOG = log.Log()

def main():
    """ the main method """
    
    usage = "USAGE: %prog <tree>"
    
    parser = OptionParser(usage)
    
    parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
                      default = False, help = "be verbose?")
    parser.add_option("-f", "--file-format", action = "store", dest = "file_format",
                      default = "phyloxml", help = "file format of the tree file")
    parser.add_option("-n", "--name", action = "store", dest = "name",
                      default = "diploid", help = "name of the diploid")
    
    
    
    (options, args) = parser.parse_args()
   
    if len(args) != 1:
        parser.error("incorrect number of arguments")

    LOG.is_logging = options.verbose
    
    input_file = args[0]

    if not os.path.exists(input_file):
        print >> sys.stderr, "Input file does not exist!"
        sys.exit(1)
    

    trees = Bio.Phylo.parse(input_file, options.file_format)
    for t in trees:
        path = t.get_path(target=options.name)
        precursor = ""
        if path == None:
            print >> sys.stderr, ("Diploid '%s' not found!" % options.name)
        elif len(path) == 1:
            precursor = t.root.name
        else:
            precursor = path[-1].name
        print precursor


if __name__ == '__main__':
    sys.exit(main())
   

  