################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../fst_align_log.cpp \
../fst_align_std.cpp \
../fst_library.cpp \
../mle_with_decoding.cpp \
../n_step_composition.cpp \
../nearest_sequence.cpp \
../normalize_alphabet.cpp 

OBJS += \
./fst_align_log.o \
./fst_align_std.o \
./fst_library.o \
./mle_with_decoding.o \
./n_step_composition.o \
./nearest_sequence.o \
./normalize_alphabet.o 

CPP_DEPS += \
./fst_align_log.d \
./fst_align_std.d \
./fst_library.d \
./mle_with_decoding.d \
./n_step_composition.d \
./nearest_sequence.d \
./normalize_alphabet.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


