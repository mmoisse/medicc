/*
 * test.cpp
 *
 *  Created on: 10 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include "fst_library.h"
#include "fst/matcher.h"
#include "fst/encode.h"

using namespace fst;

typedef StdArc Arc;
//typedef VectorFst<Arc> F;

int main(int argc, char** argv) {
  if (argc>3) {
    cerr << "USAGE: " << argv[0] << " <in:input_fst> [<in:codex]" << endl;
    return 1;
  }

  // Reads in the fst
  VectorFst<Arc>* input = VectorFst<Arc>::Read(argv[1]);
  if (input==NULL) {
    cerr << "ERROR reading fst file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read fst file." << endl;
  }

  // this is the output transducer
  VectorFst<Arc> result;

  // this is the temporary output transducer which is later deleted;
  VectorFst<Arc> result_tmp;

  // count states
  long state_count=0;
  for (StateIterator< VectorFst<Arc> > siter1(*input); !siter1.Done(); siter1.Next()) {
	  state_count++;
  }
  cerr << "States counted: " << state_count << endl;

  //////////////////////// create result fst structure //////////////////////////////////////////////////////
  // iterate over all states and create states in new fst
  Arc::StateId** state_map = new Arc::StateId*[state_count];
  for (int i=0;i<state_count;i++) {
	  state_map[i]=new Arc::StateId[state_count];
  }

  //Arc::StateId state_map[state_count][state_count];

  for (StateIterator< VectorFst<Arc> > siter1(*input); !siter1.Done(); siter1.Next()) {
		Arc::StateId state_id1 = siter1.Value();
		for (StateIterator< VectorFst<Arc> > siter2(*input); !siter2.Done(); siter2.Next()) {
			Arc::StateId state_id2 = siter2.Value();
			Arc::StateId new_state = result.AddState();
			result_tmp.AddState(); // will have same id
			Arc::Weight weight1 = input->Final(state_id1);
			Arc::Weight weight2 = input->Final(state_id2);
			if (( weight1 != Arc::Weight::Zero()) && (weight2 != Arc::Weight::Zero())) {
				result.SetFinal(new_state, Times(weight1, weight2));
				result_tmp.SetFinal(new_state, Times(weight1, weight2));
				cerr << "State " << new_state << " is final with weight " << Times(weight1, weight2) << endl;
			}
			state_map[state_id1][state_id2] = new_state;
		}
  }

  cerr << "Input start state: " << input->Start() << endl;
  Arc::StateId start_state = state_map[input->Start()][input->Start()];
  cerr << "Result start state: " << start_state << endl;
  result.SetStart(start_state);
  result.SetInputSymbols(input->InputSymbols());
  result.SetOutputSymbols(input->OutputSymbols());

  result_tmp.SetStart(start_state);
  result_tmp.SetInputSymbols(input->InputSymbols());
  result_tmp.SetOutputSymbols(input->OutputSymbols());

  cerr << "States created" << endl;
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////

  // we now need to combine all transitions of a transducer with itself (kind of a cross product). We then need
  // to set up two fsts, one for the input labels of the two sequences, one for the output labels. Those are then
  // encoded and combined into one transducer, after which we can ignore the result_tmp.

  //////////////////////////// create result fst transitions ////////////////////////////////////////////////
  // iterate over all states
  const SymbolTable* syms = input->InputSymbols(); // input and output tables should be identical

  long chrsep = syms->Find("X");
  for (StateIterator< VectorFst<Arc> > siter1(*input); !siter1.Done(); siter1.Next()) {
		Arc::StateId state_id1 = siter1.Value();
		cerr << "Current outer state: " << state_id1 << endl;
		for (ArcIterator< VectorFst<Arc> > aiter1(*input, state_id1); !aiter1.Done(); aiter1.Next()) {
			const Arc &arc1 = aiter1.Value();
			for (StateIterator< VectorFst<Arc> > siter2(*input); !siter2.Done(); siter2.Next()) {
				Arc::StateId state_id2 = siter2.Value();
				for (ArcIterator< VectorFst<Arc> > aiter2(*input, state_id2); !aiter2.Done(); aiter2.Next()) {
				  const Arc &arc2 = aiter2.Value();
				  Arc::StateId state_from = state_map[state_id1][state_id2];
				  Arc::StateId state_to = state_map[arc1.nextstate][arc2.nextstate];
				  // only add arc if either none or all 4 of the labels are X
				  if ((arc1.ilabel == chrsep && arc2.ilabel == chrsep) || (arc1.ilabel != chrsep && arc2.ilabel != chrsep)) {
					  result.AddArc(state_from, Arc(arc1.ilabel, arc2.ilabel, Times(arc1.weight, arc2.weight), state_to));
					  result_tmp.AddArc(state_from, Arc(arc1.olabel, arc2.olabel, Times(arc1.weight, arc2.weight), state_to));

					  //cerr << "Transition from (" << state_id1 << "," << state_id2 << ") to (" << arc1.nextstate << "," << arc2.nextstate << ") with ["
					//	  << syms->Find(arc1.ilabel) << syms->Find(arc2.ilabel) << "]:[" << syms->Find(arc1.olabel) << syms->Find(arc2.olabel) << "] / " << Times(arc1.weight, arc2.weight) << endl;
				  }
				}
			}
		}
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////

  // now encode them both
  cerr << "Encoding...";
  EncodeMapper<Arc> *encoder = NULL;
  if (argc==3) { // read encode table
	  cerr << "reading codex '" << argv[2] << "'...";
	  encoder = EncodeMapper<Arc>::Read(argv[2]);
  } else { // create new table
	  encoder = new EncodeMapper<Arc>(kEncodeLabels, ENCODE);
  }
  Encode(&result, encoder);
  Encode(&result_tmp, encoder);
  encoder->Write("codex");
  cerr << "done (Codex file is 'codex')" << endl;

  // now transfer the labels from the temp fst to the output of the result fst
  cerr << "Transferring labels";

  for (StateIterator< VectorFst<Arc> > siter1(result); !siter1.Done(); siter1.Next()) {
		Arc::StateId state_id1 = siter1.Value();
		//cerr << "Current outer state: " << state_id1 << endl;
		ArcIterator< VectorFst<Arc> > aiter2(result_tmp, state_id1);
		MutableArcIterator< VectorFst<Arc> > aiter1(&result, state_id1);
		for (; !aiter1.Done(); aiter1.Next()) {
			Arc arc1 = aiter1.Value();
			const Arc &arc2 = aiter2.Value();
			arc1.olabel = arc2.ilabel; // could be olabel as well of arc2, both are acceptors at that point
			aiter1.SetValue(arc1);
			if (!aiter2.Done()) {
				aiter2.Next();
			}
		}
  }
  cerr << "...done!" << endl;

  result.Write("");



}
