/*
 * normalize_alphabet.cpp
 *
 *  Created on: 7 Sep 2010
 *      Author: rfs
 */

#include "fst_library.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>

typedef LogArc Arc;

int main(int argc, char** argv) {
	if (argc != 2) {
		cerr << "USAGE: " << argv[0] << " <fst>" << endl;
		return 1;
	}


	VectorFst<Arc>* fst = VectorFst<Arc>::Read(argv[1]);

	// input sort, necessary for normalize
	ArcSort(fst, ILabelCompare<LogArc>());

	normalize_alphabet(fst);
	cerr << "end normalize" << endl;
	fst->Write("");




}

