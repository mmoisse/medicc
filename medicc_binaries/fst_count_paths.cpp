/*
 * fst_count_paths.cpp
 *
 *  Created on: 19 Nov 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>
#include "fst_library.h"

using namespace fst;

typedef VectorFst<LogArc> LogVectorFst;

long pathCount(const StdVectorFst &sfst) {
   // if the fst has loops, then the language or relation
   // encoded by the FST is infinite, just return -1
   if (sfst.Properties(kCyclic, true)) {
      return -1 ;
   }
   // copy the FST input to a new unweighted network,
   // over the Log Semiring
   LogVectorFst lfst ;

   // for each original StdArc, if the weight is _not_
   // TropicalWeight::Zero() then change it to LogWeight::One(),
   // else (when the weight is TropicalWeight::Zero()) change it to
   // LogWeight::Zero().  This is handily done with
   // RmWeightMapper(), which is in the library.

   Map(sfst, &lfst, RmWeightMapper<StdArc, LogArc>()) ;

   vector<LogArc::Weight> distance ;
   ShortestDistance(lfst, &distance, true) ;

   // from the distance vector, get the weight for the start state
   LogArc::Weight w = distance[lfst.Start()] ;

   // w.Value() is the -log of the number of paths
   // cout << "Weight from pathCount(): " << w << endl ;
   // so make w positive and get the exp()
   cerr << w.Value() << endl;
   long paths = round(exp((double)(-1.0 * w.Value()))) ;
   // cout << "Paths from pathCount(): " << paths << endl ;
   return paths ;
}

int main(int argc, char** argv) {
  if (argc!=2) {
    cerr << "USAGE: " << argv[0] << " <in:input_fst>" << endl;
    return 1;
  }

  // Reads in an input FST.
  VectorFst<StdArc> *input = VectorFst<StdArc>::Read(argv[1]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  }

  cout << pathCount(*input) << endl;


}
