#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include <map>
#include "fst_library.h"
#include <lemon/list_graph.h>

using namespace fst;
using namespace lemon;

typedef StdArc Arc;

int main(int argc, char** argv) {
  if (argc!=3) {
    cerr << "USAGE: " << argv[0] << "<in:input_fsa> <in:weight>" << endl;
    return 1;
  }

  // Reads in the transducer.
  VectorFst<Arc>* fst = VectorFst<Arc>::Read(argv[1]);
  if (fst==NULL) {
    cerr << "ERROR reading fst file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read fst file." << endl;
  }

  //read in weight
  int weight = atoi(argv[2]);

  // convert to LEMON format
  ListDigraph g;
  ListDigraph::ArcMap<int> weights(g);
  map<Arc::StateId, ListDigraph::Node> node_map;

  for (StateIterator< VectorFst<Arc> > siter(*fst); !siter.Done(); siter.Next()) {
	  Arc::StateId state_id = siter.Value();
	  ListDigraph::Node node = g.addNode();
	  node_map[state_id] = node;
  }

  for (StateIterator< VectorFst<Arc> > siter(*fst); !siter.Done(); siter.Next()) {
	  Arc::StateId state_id = siter.Value();
	  for (ArcIterator<VectorFst<Arc> > aiter(*fst, state_id); !aiter.Done(); aiter.Next()) {
		  const Arc &arc = aiter.Value();
		  ListDigraph::Arc lemon_arc = g.addArc(node_map[state_id], node_map[arc.nextstate]);
		  weights[lemon_arc] = (int)arc.weight.Value();
	  }
  }

  // solve optimization problem


}
