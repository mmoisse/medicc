/*
 * fst_sp_std.cpp
 *
 *  Created on: 26 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>
#include "fst_library.h"

using namespace fst;

typedef StdArc Arc;


int main(int argc, char** argv) {
  if (argc!=4 && argc!=5) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fst> <in:output_fst> [nshortest]" << endl;
    return 1;
  }

  cerr << "-----------------------------------------------------------" << endl;
  cerr << " Aligning " << argv[2] << " to " << argv[3] << "." << endl;
  cerr << "-----------------------------------------------------------" << endl;

  // Reads in the transduction model.
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }

  // Reads in an input FST.
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[2]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read input file." << endl;
  }

  // Reads in the output FST
  VectorFst<Arc> *output = VectorFst<Arc>::Read(argv[3]);
  if (output==NULL) {
    cerr << "ERROR reading output file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read output file." << endl;
  }

  VectorFst<Arc> path;
  if (argc==4) {
	  shortest_path(model, input, output, &path);
  } else if (argc==5) {
	  int nshortest = atoi(argv[4]);
	  nshortest_path(model, input, output, &path, nshortest, 0.0);
  }

  path.Write("");

}
