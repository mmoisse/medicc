#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include "fst_library.h"

#define PRUNING_LEVEL 4
#define CLUSTER_SIZE 3
#define BIG_NUMBER 100000

using namespace fst;

typedef LogArc Arc;

int main(int argc, char** argv) {
  if (argc<2) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:stationary_fsa> <in:input_fst1> [<...>] [<in:input_fstN>] " << endl;
    return 1;
  }

  // Reads in the transduction model. 
  VectorFst<Arc>* model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }
  
  // Reads in the stationary distribution
  VectorFst<Arc>* stationary = VectorFst<Arc>::Read(argv[2]);
  if (model==NULL) {
    cerr << "ERROR reading stationary distribution. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read stationary distribution." << endl;
  }

  // Reads in the input sequence FSTs. 
  int nseqs = argc-3;
  VectorFst<Arc>* inputs[nseqs];

  for (int i=3; i<argc; i++) {
    cerr << " Reading " << argv[i] << endl;
    inputs[i-3] = VectorFst<Arc>::Read(argv[i]);
    if (inputs[i-3]==NULL) {
      cerr << " ERROR reading sequence file " << argv[i] << endl;
      return 1;
    }
  }

  // now compose the model with the FSTs, project to input and sort
  ArcSort(model, OLabelCompare<Arc>());
  
  //ComposeFstOptions<Arc> co;
  //co.gc_limit=0;
  ComposeOptions opts(true, AUTO_FILTER);

  VectorFst<Arc>* compositions = new VectorFst<Arc>[nseqs];

  for (int i=0; i<nseqs; i++) {
    cerr << "Composing model with input " << argv[i+3] << "..." << flush;
//    VectorFst<Arc>* tmp = new VectorFst<Arc>();
//    cerr << "1." << flush;
//    Compose(*model, *inputs[i], tmp, opts);
//    cerr << "2." << flush;
//    Compose(*tmp, *stationary, &compositions[i], opts);
//    delete(tmp);
    cerr << "1.2." << flush;
    Compose(*model, *inputs[i], &compositions[i]);
    cerr << "3." << flush;
    Project(&compositions[i], PROJECT_INPUT);
    cerr << "4." << flush;
    ArcSort(&compositions[i], OLabelCompare<Arc>());
    cerr << "done!" << endl;
  }
  //delete(inputs);

  // decoding and determinizing the FSAs
  VectorFst<Arc>* decodings = new VectorFst<Arc>[nseqs];
  cerr << "Decoding the FSTs" << flush;
  for (int i=0; i<nseqs; i++) {
    VectorFst<Arc> decoded;
    decode(&compositions[i], &decoded);
    Determinize(decoded, &decodings[i]);
    decodings[i].Write(string(argv[i+2]) + string(".decode"));
    cerr << "." << flush;
  }
  cerr << "done!" << endl;
  //delete(compositions);

  // intersecting sequences
  cerr << "Intersecting the sequences" << flush;
  VectorFst<Arc>* final = new VectorFst<Arc>();
  if (nseqs > 1) {
    Intersect(decodings[0], decodings[1], final);
    cerr << ".." << flush;
  } else {
    final = &decodings[0];
  }
  for (int i=2; i<nseqs; i++) {
    VectorFst<Arc>* tmp = new VectorFst<Arc>();
    Intersect(*final, decodings[i], tmp);
    delete(final);
    final = tmp;
    cerr << "." << flush;
  }
  cerr << "done!" << endl;

  //delete(decodings);

  final->Write("");

  // calculating shortest path
//   cerr << "Calculating shortest path" << flush;
//   vector<Arc::Weight> distance;
//   NaturalShortestFirstQueue<Arc::StateId, Arc::Weight> state_queue(distance);
//   ShortestPathOptions<Arc, NaturalShortestFirstQueue<Arc::StateId, Arc::Weight>, AnyArcFilter<Arc> > sp_opts(&state_queue, AnyArcFilter<Arc>(), 1, false, false, mydelta);
//   sp_opts.first_path=true;
  
//   VectorFst<Arc> shortest_path;
//   ShortestPath(*final, &shortest_path, &distance, sp_opts);
//   shortest_path.Write("");

  return 0;
  
}
