/*
 * fst_offset_weights.cpp
 *
 *  Created on: 28 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>
#include "fst_library.h"

using namespace fst;

typedef StdArc Arc;


int main(int argc, char** argv) {
  if (argc!=3) {
    cerr << "USAGE: " << argv[0] << " <in:input_fst> <in:offset>" << endl;
    return 1;
  }

  // Reads in an input FST.
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[1]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  }

  float offset = atof(argv[2]);


  //float pathlength = atoi(argv[3]);

  OffsetMapper mapper = OffsetMapper(offset);

  Map(input, &mapper);

  input->Write("");

}
