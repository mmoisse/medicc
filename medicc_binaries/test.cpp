/*
 * test.cpp
 *
 *  Created on: 10 Oct 2010
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include "fst_library.h"
#include "fst/matcher.h"
#include "fst/encode.h"
#include "fst_library.h"

//#include "real-weight.h"

using namespace fst;

//typedef VectorFst<Arc> F;
//typedef ArcTpl<RealWeight> RealArc;
static VectorFst<StdArc>* ed = VectorFst<StdArc>::Read("/home/rfs/Documents/Projects/CRI/project-cnv-distance/ed/ed_asymm_ls.bin");

ProjectFst<StdArc>* project(Fst<StdArc>& fst) {
	ArcSortFst<StdArc, OLabelCompare<StdArc> > ed_sorted = ArcSortFst<StdArc, OLabelCompare<StdArc> >(*ed, OLabelCompare<StdArc>());
	ComposeFst<StdArc> comp = ComposeFst<StdArc>(ed_sorted, fst);
	ProjectFst<StdArc>* result = new ProjectFst<StdArc>(comp, PROJECT_INPUT);
	return result;
}

IntersectFst<StdArc>* intersect(const Fst<StdArc> &fst1, const Fst<StdArc> &fst2) {
	ArcSortFst<StdArc, OLabelCompare<StdArc> > fst1_sorted = ArcSortFst<StdArc, OLabelCompare<StdArc> >(fst1, OLabelCompare<StdArc>());
	IntersectFst<StdArc>* result = new IntersectFst<StdArc>(fst1_sorted, fst2);
	return result;
}

int main(int argc, char** argv) {


	// Reads in the fst
	VectorFst<StdArc>* t0 = VectorFst<StdArc>::Read("taxon_0_resolved_dec_A.bin");
	VectorFst<StdArc>* t1 = VectorFst<StdArc>::Read("taxon_1_resolved_dec_A.bin");
	VectorFst<StdArc>* t2 = VectorFst<StdArc>::Read("taxon_2_resolved_dec_A.bin");
	VectorFst<StdArc>* t3 = VectorFst<StdArc>::Read("taxon_3_resolved_dec_A.bin");
	VectorFst<StdArc>* t4 = VectorFst<StdArc>::Read("taxon_4_resolved_dec_A.bin");
	VectorFst<StdArc>* t5 = VectorFst<StdArc>::Read("taxon_5_resolved_dec_A.bin");
	VectorFst<StdArc>* t6 = VectorFst<StdArc>::Read("taxon_6_resolved_dec_A.bin");
	VectorFst<StdArc>* t7 = VectorFst<StdArc>::Read("taxon_7_resolved_dec_A.bin");
	VectorFst<StdArc>* diploid = VectorFst<StdArc>::Read("diploid_resolved_dec_A.bin");

	IntersectFst<StdArc>* int1 = intersect(*project(*t4), *project(*t6));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int3 = intersect(*project(*int1), *project(*t2));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int2 = intersect(*project(*t0), *project(*t5));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int4 = intersect(*project(*int2), *project(*int3));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int6 = intersect(*project(*int4), *project(*t3));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int7 = intersect(*project(*int6), *project(*t1));
	cerr << "x" << endl;
	IntersectFst<StdArc>* int5 = intersect(*project(*int7), *project(*t7));
	cerr << "x" << endl;

	cerr << "shortest path running" << endl;
	VectorFst<StdArc> int5_est;
	//shortest_path(ed, diploid, int5, &int5_est);

	int5_est.Write("");

}

