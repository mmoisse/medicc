#!/usr/bin/python

import fst
import fst.io
import fst.factory

with open("ed_asymm_ls.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_ls = reader.create_fst()

with open("ed_asymm_nls.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls = reader.create_fst()

with open("ed_asymm_nls_xt.fst","r") as fd:
    reader = fst.io.OpenFstTextReader(fd)
    reader.read()
    fst_nls_xt = reader.create_fst()


for state in fst_ls.states:
    fst_ls.add_transition((state.id, state.id, '0', '0', 0))

## duplications
alphabet=["0","1","2","3","4","5","6"]
state=fst_nls.add_state()
fst_nls.add_transitions([(0, state, s, t, 0) for s in alphabet for t in alphabet if fst.factory.FSTFactory._hex_is_double(s, t)]) ## event open transitions
fst_nls.add_transitions([(state, state, s, t, 0) for s in alphabet for t in alphabet if fst.factory.FSTFactory._hex_is_double(s, t)]) ## event extend transitions
fst_nls.add_transition((state, state, '0', '0', 0))
#fst_nls.add_transitions([(state, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions

state=fst_nls.add_state()
fst_nls.add_transitions([(0, state, s, t, 0) for s in alphabet for t in alphabet if fst.factory.FSTFactory._hex_is_double(t, s)]) ## event open transitions
fst_nls.add_transitions([(state, state, s, t, 0) for s in alphabet for t in alphabet if fst.factory.FSTFactory._hex_is_double(t, s)]) ## event extend transitions
fst_nls.add_transition((state, state, '0', '0', 0))
#fst_nls.add_transitions([(state, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions
    

for state in fst_nls.states:
    fst_nls.add_transition((state.id, state.id, '0', '0', 0))

for state in fst_nls_xt.states:
    fst_nls_xt.add_transition((state.id, state.id, '0', '0', 0))

with open("ed_asymm_ls.fst","w") as fd:
    fd.write(str(fst_ls))

with open("ed_asymm_nls.fst","w") as fd:
    fd.write(str(fst_nls))

with open("ed_asymm_nls_xt.fst","w") as fd:
    fd.write(str(fst_nls_xt))




