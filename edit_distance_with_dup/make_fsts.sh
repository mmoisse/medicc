#!/bin/bash

create_cnv_fst.py -c m ed_asymm_nls_step1.fst -a 0123456
fstmake.sh ed_asymm_nls_step1.fst ../cnv.symbols > ed_asymm_nls_step1.bin 
fst_multicmd.py -c fstcompose -r 7 ed_asymm_nls_step1.bin -ms > ed_asymm_nls.bin
fstprint ed_asymm_nls.bin > ed_asymm_nls.fst

create_cnv_fst.py -c l ed_asymm_ls_step1.fst -a 0123456
fstmake.sh ed_asymm_ls_step1.fst ../cnv.symbols > ed_asymm_ls_step1.bin 
fst_multicmd.py -c fstcompose -r 7 ed_asymm_ls_step1.bin -ms > ed_asymm_ls.bin
fstprint ed_asymm_ls.bin > ed_asymm_ls.fst

create_cnv_fst.py -c m ed_asymm_nls_step1_xt.fst -a 0123456789a
fstmake.sh ed_asymm_nls_step1_xt.fst ../cnv.symbols > ed_asymm_nls_step1_xt.bin 
fst_multicmd.py -c fstcompose -r 15 ed_asymm_nls_step1_xt.bin -ms > ed_asymm_nls_xt.bin
fstprint ed_asymm_nls_xt.bin > ed_asymm_nls_xt.fst

./modify_fsts.py

fstmake.sh ed_asymm_nls.fst ../cnv.symbols > ed_asymm_nls.bin
fstmake.sh ed_asymm_ls.fst ../cnv.symbols > ed_asymm_ls.bin
fstmake.sh ed_asymm_nls_xt.fst ../cnv.symbols > ed_asymm_nls_xt.bin

fstinvert ed_asymm_nls.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls.bin > ed_symm_nls_large.bin
fstencode_and_minimize.sh ed_symm_nls_large.bin > ed_symm_nls.bin
#cp ed_symm_nls_large.bin ed_symm_nls.bin

fstinvert ed_asymm_nls_xt.bin | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls_xt.bin > ed_symm_nls_large_xt.bin
fstencode_and_minimize.sh ed_symm_nls_large_xt.bin > ed_symm_nls_xt.bin

productfst ../edit_distance_stable_incl_chr_seperator_scott3/ed_symm_nls.bin > ed_symm_dual.bin 2>/dev/null
mv codex codex_symm
productfst ../edit_distance_stable_incl_chr_seperator_scott3/ed_asymm_nls.bin > ed_asymm_dual.bin 2>/dev/null
mv codex codex_asymm

