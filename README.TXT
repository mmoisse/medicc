#### MEDICC - Minimum Event Distance for Intra-tumour Copy number Comparisons ####


Download:
* Pull the MEDICC repository using "git pull https://rfs@bitbucket.org/rfs/medicc.git"
* Navigate into the "medicc/lib" subfolder and pull the FSTFramework respository using "git pull https://rfs@bitbucket.org/rfs/medicc.git"

Other requirements:
* Unix style operating system or Cygwin on Windows
* Python 2.7x with Biopython, Numpy/Scipy, weblogolib 3.3 (the latter should be available via "easy_install weblogo==3.3")

! Make sure you use weblogolib 3.3 for now, not the new 3.4 version in which function calls have changed without it being mentioned in the changelog.
! There is apparently an incompatibility between weblogolib 3.3 and Ghostscript 9.10 as included in the latest Ubuntu release. Before weblogolib 3.4 is the default
! version on all platforms (my Windows easy_install still pulls 3.3 by default and doesn't find 3.4), there is little we can do. Should you encounter runtime
! errors that result from the weblogolib, please run medicc with the --disable-logo flag to prevent import and execution of weblogolib related code. As a result
! no weblogos will be created for the ancestral state reconstruction.

* OpenFST 1.3.2 (http://www.openfst.org) compiled and in your PATH. You must ./configure OpenFST with at least the --enable-pdt and --enable-far flags.

! Use this exact version of OpenFST, in particular the new OpenFST versions do not seem to compile on cygwin anymore.

* Phylip (http://evolution.genetics.washington.edu/phylip.html) compiled and in your PATH

Installation:
* Navigate to the "medicc_binaries" subfolder, create binary folder using "mkdir bin" if it does not exist, then compile using "make" under Cygwin or Unix
* Navigate to the FST Framework/cExtensions subfolder (e.g lib/fstframework/cExtensions), compile using "make" under Cygwin or Unix

! The OpenFST make files install parts of the OpenFST shared libraries in a "fst" subfolder under the chosen install prefix, by default /usr/local/lib/fst 
! You need to make sure this folder is part of both the runtime link search path (e.g. add it to ldconfig) as well as the compile time link search path.
! For the latter there is a variable in the cExtensions makefile that sets the -L flag that is passed to the linker. It is by default set to /usr/local/lib/fst.
! In case of a custom install path to the OpenFST libraries this makefile might need editing depending on your flavor of *nix and your system configuration.

* Main executable "medicc.py" takes two arguments (+options):
	1) Description file indicating the position of the major and minor CNV sequences
	2) Output directory
  For an example see the "examples" subfolder

Test:

To check the installation run from the medicc folder

./medicc.py --help

If that succeeds, run the test examples using:

./medicc.py examples/example1/desc.txt examples/example1.out -v

In case of problems or questions, contact me at rfs32@cam.ac.uk


July 2014, Roland Schwarz