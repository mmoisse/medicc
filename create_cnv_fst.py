#!ENV python
""" Create kernel fsts for CNV project (separate signal and noise parts) """

import sys
from optparse import OptionParser
from fst.fst import *
from fst.factory import FSTFactory

 

usage = "USAGE: %prog <fst_file>"

parser = OptionParser(usage)

parser.add_option("-s", "--symbol-file", action = "store", type = "string", dest = "symbol_file", default = "", metavar = "FILE", 
                  help = "write symbol file to FILE")
parser.add_option("-p", "--parentheses-file", action = "store", type = "string", dest = "parens_file", default = "", metavar = "FILE", 
                  help = "write parentheses file to FILE")
parser.add_option("-n", "--nparentheses", action = "store", type = "int", dest = "nparens", default = 4, metavar = "FILE", 
                  help = "number of parentheses pairs to generate (min 4)")
parser.add_option("-c", "--choice", action = "store", dest = "choice", default = "c", 
                  help = "which fst to create: symmetric edit distance FST without length scoring (d),\
                  asymmetric FST with length scoring (l), asymmetric FST without length scoring (m),\
                   asymmetric FST without length scoring [deletions only] (md), experimental LOH edit distance (x)")
parser.add_option("-a", "--alphabet", action = "store", dest = "alphabet", default = "", 
                  help = "alphabet to use if not 0:f")
parser.add_option("-x", "--separator", action="store_true", dest = "separator", default = False, help = "add separator")
parser.add_option("-z", "--zeros", action="store_true", dest = "zeros", default = False, help = "add zero transitions")


(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("incorrect number of arguments")

if options.alphabet == "":
    alphabet = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')
else:
    alphabet = [s for s in options.alphabet] 

if options.choice == "d":
    fst_signal = FSTFactory.create_cnv_fst_symmetric_no_lengthscoring(alphabet)
elif options.choice == "l":
    fst_signal = FSTFactory.create_cnv_fst_asymm_lengthscoring(alphabet, separator= ("X" if options.separator else None), add_zeros=options.zeros)
elif options.choice == "m":
    fst_signal = FSTFactory.create_cnv_fst_asymm_no_lengthscoring(alphabet, separator=("X" if options.separator else None), add_zeros=options.zeros)
elif options.choice == "md":
    fst_signal = FSTFactory.create_cnv_fst_asymm_no_lengthscoring(alphabet, separator=("X" if options.separator else None), add_zeros=options.zeros, deletions_only=True)
elif options.choice == "x":
    fst_signal = FSTFactory.create_cnv_fst_asymm_loh_only(alphabet, separator=("X" if options.separator else None))
elif options.choice == "duplicate":
    fst_signal = FSTFactory.create_cnv_fst_asymm_nls_duplications(alphabet, separator=("X" if options.separator else None), add_zeros=options.zeros)

try:
    with open(args[0], "w") as fd:
        fd.write(str(fst_signal))
except IOError:
    print "ERROR: Couldn't write signal FST file %s. Exiting." % args[0]

if options.symbol_file != "":
    try:
        with open(options.symbol_file, "wb") as fd:
            fd.write(fst_signal.create_symbol_table())
    except IOError:
        print "ERROR: Couldn't write FST symbol file. Exiting."

if options.parens_file != "":
    nparens = max(4, options.nparens)
    ## create basic (hardcoded) parentheses
    basic_parens = ["( 100",") 101","[ 102","] 103","{ 104","} 105","< 106","> 107"]

    parens = basic_parens[:]

    for i in range(nparens-4):
        idx = 108 + i*2
        parens.append("%s %d" % ("("+hex(i+2)[1:], idx))
        parens.append("%s %d" % (")"+hex(i+2)[1:], idx+1))

    pairs = ["%d %d" % (100 + i*2, 101 + i*2) for i in range(nparens)]

    try:
        if options.symbol_file != "":
            with open(options.symbol_file, "ab") as fd:
                fd.write("\n".join(parens)+"\n")
        
        with open(options.parens_file, "wb") as fd:
            fd.write("\n".join(pairs)+"\n")
    except IOError:
        print "ERROR: Couldn't write FST symbol file or parentheses file. Exiting."
