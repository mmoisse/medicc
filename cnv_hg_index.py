#!/usr/bin/python

'''
Created on 18 Aug 2011

@author: rfs
'''

import os
import sys
from optparse import OptionParser
from Bio import Phylo
from numpy import *
import itertools
import scipy.stats

usage = "USAGE: %prog"
parser = OptionParser(usage=usage)

parser.add_option("-f", "--input-file-format", action = "store", dest = "input_file_format", 
                  default = "phyloxml", help = "input file format")


(options, args) = parser.parse_args()

if len(args) > 1:
    parser.error("incorrect number of arguments")
elif len(args)==1:
    fd_in = open(args[0], "r")   
else:
    fd_in = sys.stdin
    

############################################

trees = Phylo.parse(fd_in, options.input_file_format)

treecount = 0
for tree in trees:
    treecount += 1
    result = dict()
    ## first find total tree length
    tree_length = sum([c.branch_length for c in tree.find_clades() if c != tree.root])
    
    ## now test individual branches
    for clade in tree.find_clades():
        if clade != tree.root:
            try:
                teststat = math.log(clade.branch_length) - math.log(tree_length)
                pval = 1-scipy.stats.norm.cdf(teststat, loc = -3.483824, mu = 0.740662)
            except ValueError:
                pval = 1
            result[clade.name] = pval

def sigstring(pval):
    sigs = ""
    if pval < 0.001:
        sigs = "***"
    elif pval < 0.01:
        sigs = "**"
    elif pval < 0.05:
        sigs = "*"
    elif pval < 0.1:
        sigs = "."        
    return sigs
    
for a,b in result.iteritems():
    print "%s    %f    %s" % (a, b, sigstring(b))
                         
fd_in.close()

